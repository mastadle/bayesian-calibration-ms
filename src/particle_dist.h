#ifndef PARTICLE_DIST_H_
#define PARTICLE_DIST_H_

#include "encode.h"

#include <Eigen/Dense>
#include <vector>
#include <cmath>
#include <string>
//#include "dlib/optimization.h"
//#include "dlib/global_optimization.h"

namespace pdist {

// This is defined in encode.h, so no need to have it here
// struct Range {
//     double min;
//     double max;
// };

bool multi_norm(const int N_SAMPLES, const Eigen::VectorXd &Mu, const Eigen::MatrixXd &Sigma, Eigen::MatrixXd &samples_out);
// void testMultiNorm();

class ParticleDist {
private:
    bool m_ndims_set=false;
    bool m_Mu_set=false;
    bool m_Sigma_set=false;
protected:
    int m_ndims;
    Eigen::VectorXd m_Mu;
    Eigen::MatrixXd m_Sigma;
    const int m_MIN_PTLS;
public:
    ParticleDist() : m_MIN_PTLS{30} {}
    ParticleDist(const int MIN_PTLS) : m_MIN_PTLS{MIN_PTLS} {}
    void set_ndims(const int NDIMS);
    void set_Mu(const std::vector<double> &Mu_in);
    void set_Sigma(const std::vector<std::vector<double>> &Sigma_in);
    ParticleDist(const std::vector<double> &Mu, const std::vector<std::vector<double>> &Sigma);
    ParticleDist(const std::vector<double> &Mu, const std::vector<std::vector<double>> &Sigma, const int MIN_PTLS);
    int get_ndims() const;
    void get_Mu(std::vector<double> &Mu_out) const;
    void get_Sigma(std::vector<std::vector<double>> &Sigma_out) const;
protected:
    // virtual double get_kappa(const int RESULT) const = 0;
    virtual void get_input_point(const Eigen::VectorXd &particle,
                                 const std::vector<double> &inputs, Eigen::VectorXd &input_point_out) const = 0;
    virtual bool check_ranges(const Eigen::VectorXd &input_point) const = 0;
    // The two functions below could probably also be const, but would need to rewrite some code in cppEncode.cpp since
    // some non const functions (which could probably be const) are called on the DataProcessor class instance member
    // variables.
    virtual void get_likelihoods(const int RESULT, const Eigen::MatrixXd &InputPoints, Eigen::VectorXd &likelihoods_out) = 0;
protected:
    bool generate_particles(const int N_PTLS, const std::vector<double> &inputs,
                            Eigen::MatrixXd &InputPoints_out, Eigen::MatrixXd &Particles_out);
public:
//    virtual double get_entropy_gain(const std::vector<double> &controls) = 0;
//    virtual double get_NV(const std::vector<double> &controls) = 0;
    bool update(const std::vector<int> &results, const std::vector<double> &inputs,
                const int N_PTLS, const double KAPPA, const double UNCERTAINTY
                // for testing:
                // std::vector<std::vector<double>> &prior_particles_out,
                // std::vector<double> &particle_likelihoods_out,
                // std::vector<std::vector<double>> &posterior_particles_out
        );
//    void get_opt_controls(std::vector<double> &controls_out, const std::vector<double> &mins,
//                          const std::vector<double> &maxes, const int MAX_CALLS);
};

//class EntropyGain {
//private:
//    ParticleDist *const m_dist;
//public:
//    EntropyGain(ParticleDist *const dist);
//    double operator()(const dlib::matrix<double,0,1> &controls);
//};

//class NegativeVariance {
//private:
//    ParticleDist *const m_dist;
//public:
//    NegativeVariance(ParticleDist *const dist);
//    double operator()(const dlib::matrix<double,0,1> &controls);
//};

namespace Result {
    enum TwoQubitMeas {
        GG,
        EE,
        GEEG,
        NUM_RESULTS
    };
}

namespace Param {
    enum CalibParam {
        DELTA,
        DELTA0,
        TAU,
        NUM_PARAMS
    };
}

namespace Param2D {
    enum CalibParam {
        DELTA,
        DELTA0,
        NUM_PARAMS
    };
}

class DistExpMS : public ParticleDist {
private:
    // Eigen::VectorXd m_kappas;
    std::vector<data_comp::Range> m_ranges, m_ranges_small;
    // std::string m_filename_gg;
    // std::string m_filename_ee;
    // std::string m_filename_gg_small;
    // std::string m_filename_ee_small;
    data_comp::DataProcessor m_gg_data, m_ee_data, m_gg_data_small, m_ee_data_small;
    // bool m_kappas_set=false;
    bool m_ranges_set=false;
    bool m_gg_file_set=false;
    bool m_ee_file_set=false;
    bool m_ranges_set_small=false;
    bool m_gg_file_set_small=false;
    bool m_ee_file_set_small=false;
    // int m_gain_ptls=1000;
    const bool m_3D;
    const int m_NDIMS_CONTROLS=Param::NUM_PARAMS; // even if we are only calibrating two parameters
public:
    DistExpMS();
    DistExpMS(const int MIN_PTLS, const bool _3D);
    DistExpMS(const std::vector<double> &Mu, const std::vector<std::vector<double>> &Sigma);
    DistExpMS(const std::vector<double> &Mu, const std::vector<std::vector<double>> &Sigma, const int MIN_PTLS, const bool _3D);

//    virtual double get_entropy_gain(const std::vector<double> &controls);
//    virtual double get_NV(const std::vector<double> &controls);

    // void set_kappas(const std::vector<double> &kappas);
    void set_ranges(const std::vector<std::vector<double>> &ranges, const bool SMALL);
    void set_data(const int RESULT, const api::compressed_data &data_struct, const std::vector<int> &spacings,
                  const std::vector<double> &mins, const std::vector<double> &maxes, const bool SMALL);
    // void set_gain_particles(const int N_PTLS);
protected:
    // virtual double get_kappa(const int RESULT) const;
    virtual void get_input_point(const Eigen::VectorXd &particle,
                                 const std::vector<double> &inputs, Eigen::VectorXd &input_point_out) const;
    virtual bool check_ranges(const Eigen::VectorXd &input_point) const;
    virtual void get_likelihoods(const int RESULT, const Eigen::MatrixXd &InputPoints, Eigen::VectorXd &likelihoods_out);
};

}

#endif /* PARTICLE_DIST_H_ */
