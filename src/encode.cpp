#include "encode.h"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <random>
#include <cassert>

namespace data_comp {

// #include <chrono>

// class Timer {
// private:
//     using clock_t = std::chrono::high_resolution_clock;
//     using second_t = std::chrono::duration<double, std::ratio<1> >;

//     std::chrono::time_point<clock_t> m_beg;
// public:
//     Timer() : m_beg(clock_t::now()) {}

//     void reset() {
//         m_beg = clock_t::now();
//     }

//     double elapsed() const {
//         return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
//     }
// };

int pow_int(int base, int expon) {
    int result = 1;
    while (expon)
    {
        if (expon & 1)
            result *= base;
        expon >>= 1;
        base *= base;
    }
    return result;
}

// double rand_unif() {
//     // returns random number in [0,1)
//     static std::random_device rd;
//     static std::mt19937 mersenne{ rd() };
//     static const double fraction = 1.0 / (static_cast<double>(mersenne.max()) + 1.0);
//     return mersenne() * fraction;
// }

Node::Node() {}

Node::Node(double freq) : m_freq{freq} {}

Node::~Node() {}

char Node::get_type() const {return 'N';}

double Node::get_freq() const {return m_freq;}
Node* Node::get_parent() const {return m_parent;}
int Node::get_symbol() const {throw std::runtime_error("Node instaces don't have symbols.\n");}
void Node::get_code(std::vector<unsigned char> &code_out) const {throw std::runtime_error("Node instances don't have codes.\n");}
Node* Node::get_child(Child bit) const {throw std::runtime_error("Node instances don't have cildren.\n");}
void Node::set_freq(double freq) {m_freq = freq;}
void Node::set_parent(Node *parent) {m_parent = parent;}
void Node::set_symbol(int symbol) {throw std::runtime_error("Node instaces don't have symbols.\n");}
void Node::set_code(const std::vector<unsigned char> &code_in) {throw std::runtime_error("Node instances don't have codes.\n");}
void Node::set_child(Child bit, Node *child) {throw std::runtime_error("Node instances don't have cildren.\n");}

Leaf::Leaf() {}

Leaf::Leaf(int symbol, double freq) : Node(freq), m_symbol{symbol} {}

char Leaf::get_type() const {return 'L';}

int Leaf::get_symbol() const {return m_symbol;}
void Leaf::get_code(std::vector<unsigned char> &code_out) const {code_out = m_code;}
Node* Leaf::get_child(Child bit) const {throw std::runtime_error("Leaf instances don't have cildren.\n");}
void Leaf::set_symbol(int symbol) {m_symbol = symbol;}
void Leaf::set_code(const std::vector<unsigned char> &code_in) {m_code = code_in;}
void Leaf::set_child(Child bit, Node *child) {throw std::runtime_error("Leaf instances don't have cildren.\n");}

Branch::Branch() {}

Branch::Branch(Node *child0, Node *child1, double freq) : Node(freq), m_child0{child0}, m_child1{child1} {}

char Branch::get_type() const {return 'B';}

int Branch::get_symbol() const {throw std::runtime_error("Branch instaces don't have symbols.\n");}
void Branch::get_code(std::vector<unsigned char> &code_out) const {throw std::runtime_error("Branch instances don't have codes.\n");}
Node* Branch::get_child(Child bit) const {
    switch (bit) {
    case Child::ZERO:
        return m_child0;
    case Child::ONE:
        return m_child1;
    default:
        throw std::runtime_error("Invalid Child.\n");
    }
}
void Branch::set_symbol(int symbol) {throw std::runtime_error("Branch instaces don't have symbols.\n");}
void Branch::set_code(const std::vector<unsigned char> &code_in) {throw std::runtime_error("Branch instances don't have codes.\n");}
void Branch::set_child(Child bit, Node *child) {
    switch (bit) {
    case Child::ZERO:
        m_child0 = child;
        break;
    case Child::ONE:
        m_child1 = child;
        break;
    default:
        throw std::runtime_error("Invalid Child.\n");
    }
}

void print_bits(const std::vector<unsigned char> &bits) {
    std::cout << "Bits: ";
    for (const auto byte : bits) {
        for (const auto BIT : BITS) {
            if (byte & BIT) {
                std::cout << 1;
            } else {
                std::cout << 0;
            }
        }
    }
    std::cout << "\n";
}

void DataProcessor::allocate_variables() {
    assert(!m_allocated && "Need to deallocate member variables before a new allocation.\n");
    assert(m_shape.size() == m_block_shape.size() && "dataset and blocks must have the same number of dimensions\n");
    m_ndims = static_cast<int>(m_shape.size());
    m_size = 1; m_block_size = 1, m_num_blocks = 1;
    m_shape_blocks.resize(m_ndims);
    for (int d=0; d<m_ndims; ++d) {
        assert((m_shape[d] - 1) % (m_block_shape[d] - 1) == 0 &&
               "overall dimension size (minus 1) must be a multiple of block dimension size (minus 1)\n");
        m_shape_blocks[d] = (m_shape[d] - 1) / (m_block_shape[d] - 1);
        m_num_blocks *= m_shape_blocks[d];
        m_size *= m_shape[d];
        m_block_size *= m_block_shape[d];
    }

    // debugging
    // for (int n=0; n<static_cast<int>(m_shape.size()); ++n) {
    //     std::cout << "m_shape[" << n << "] = " << m_shape[n] << "\n";
    // }
    // for (int n=0; n<static_cast<int>(m_block_shape.size()); ++n) {
    //     std::cout << "m_block_shape[" << n << "] = " << m_block_shape[n] << "\n";
    // }
    // std::cout << "m_ndims = " << m_ndims << "\n";
    // std::cout << "m_size = " << m_size << "\n";
    // std::cout << "m_block_size = " << m_block_size << "\n";
    // std::cout << "m_num_blocks = " << m_num_blocks << "\n";
    // for (int n=0; n<static_cast<int>(m_shape_blocks.size()); ++n) {
    //     std::cout << "m_shape_blocks[" << n << "] = " << m_shape_blocks[n] << "\n";
    // }
    m_allocated = true;
}

DataProcessor::DataProcessor(const std::vector<int> &shape, const std::vector<int> &block_shape) :
    m_shape{shape}, m_block_shape{block_shape}
{
    allocate_variables();
}

void DataProcessor::deallocate_variables() {
    assert(m_allocated && "Need to allocate before deallocating.\n");
    for (int I=0; I<m_num_blocks; ++I) {
        if (m_got_dct) {
            delete[] m_block_data[I];
            delete[] m_dcts[I];
            if (I == m_num_blocks-1) m_got_dct = false;
        }
        if (m_compressed) {
            delete[] m_dcts_lossy[I];
            if (I == m_num_blocks-1) m_compressed = false;
        }
        if (m_quantized) {
            delete[] m_dcts_quant[I];
            if (I == m_num_blocks-1) m_quantized = false;
        }
        if (m_decoded) {
            delete[] m_dcts_decoded[I];
            if (I == m_num_blocks-1) m_decoded = false;
        }
    }
    m_allocated = false;
}

DataProcessor::~DataProcessor() {
    if (m_allocated) deallocate_variables();
}

void DataProcessor::add_dataset(const std::vector<double> &dataset) {
    assert(m_allocated && "Need to allocate variables before adding a dataset.\n");
    assert(static_cast<int>(dataset.size()) == m_size && "Input dataset size doesn't match the size allocated by DataProcessor.\n");
    m_dataset = dataset;
    m_got_data = true;
}

void set_index_vector(const int index_in, std::vector<int> &indices_out, const std::vector<int> &shape) {
    int SIZE {1}, ndims{static_cast<int>(indices_out.size())};
    for (int d=1; d<ndims; ++d) {
        SIZE *= shape[d];
    }
    int i {index_in};
    for (int d=0; d<ndims-1; ++d) {
        indices_out[d] = i/SIZE;
        if (d == ndims-2) {
            indices_out[d+1] = i%SIZE;
        } else {
            i = i%SIZE;
            SIZE /= shape[d+1];
        }
    }
}

int get_index(const std::vector<int> &indices_in, const std::vector<int> &shape) { // essentially the inverse mapping of set_index_vector
    const int NDIMS {static_cast<int>(indices_in.size())};
    int index_out {0}, SIZE {1};
    for (int d=0; d<NDIMS; ++d) {
        SIZE *= shape[d];
    }
    for (int d=0; d<NDIMS; ++d) {
        SIZE /= shape[d];
        index_out += indices_in[d]*SIZE;
    }
    return index_out;
}

void DataProcessor::compute_dct() {
    assert(m_got_data && "Need to add dataset before computing DCT.\n");

    m_block_data.resize(m_num_blocks);
    m_dcts.resize(m_num_blocks);
    for (int I=0; I<m_num_blocks; ++I) {
        m_block_data[I] = new double[m_block_size];
        m_dcts[I] = new double[m_block_size];
    }

    fftw_plan dct_plan;
    int *nnn = new int[m_ndims];
    fftw_r2r_kind *kind = new fftw_r2r_kind[m_ndims];
    for (int d=0; d<m_ndims; ++d) {
        nnn[d] = m_block_shape[d];
        kind[d] = FFTW_REDFT10;
    }
    std::vector<int> l_vec, k_vec, n_vec;
    l_vec.resize(m_ndims); k_vec.resize(m_ndims); n_vec.resize(m_ndims);
    int i;
    for (int I=0; I<m_num_blocks; ++I) {
        dct_plan = fftw_plan_r2r(m_ndims, nnn, m_block_data[I], m_dcts[I], kind, FFTW_MEASURE);
        // copy data into block
        set_index_vector(I, l_vec, m_shape_blocks);
        for (int j=0; j<m_block_size; ++j) {
            // find index i of m_dataset corresponding to (I, j)
            set_index_vector(j, k_vec, m_block_shape);
            for (int d=0; d<m_ndims; ++d) {
                // for non-overlapping blocks use the line below instead (and need to adjust the requirements in
                // allocate_variables()):
                // n_vec[d] = l_vec[d]*m_block_shape[d] + k_vec[d];
                n_vec[d] = l_vec[d]*(m_block_shape[d]-1) + k_vec[d];
            }
            i = get_index(n_vec, m_shape);

            // assign data value to block data
            m_block_data[I][j] = m_dataset[i];

            // debugging
            // std::cout << "I: " << I << "\n";
            // std::cout << "j: " << j << "\n";
            // std::cout << "l_vec: ";
            // for (int d=0; d<m_ndims; ++d) {
            //     std::cout << l_vec[d] << " ";
            // }
            // std::cout << "\n";
            // std::cout << "k_vec: ";
            // for (int d=0; d<m_ndims; ++d) {
            //     std::cout << k_vec[d] << " ";
            // }
            // std::cout << "\n";
            // std::cout << "i: " << i << "\n";
        }

        fftw_execute(dct_plan);
        for (int j=0; j<m_block_size; ++j) {
            m_dcts[I][j] /= static_cast<double>(m_block_size)*pow(2.0, m_ndims);
        }
    }
    delete[] nnn;
    delete[] kind;
    m_got_dct = true;
}

void DataProcessor::compress_dct(const std::vector<int> &shape_to_keep) {
    // naive implementation (just set some high frequency components to zero) for initial testing
    assert(m_got_dct && "Need to compute dct before compressing dct\n");
    assert(static_cast<int>(shape_to_keep.size()) == m_ndims && "shape_to_keep doesn't match the dimensions of the data\n");

    m_dcts_lossy.resize(m_num_blocks);
    for (int I=0; I<m_num_blocks; ++I) {
        m_dcts_lossy[I] = new double[m_block_size];
    }

    std::vector<int> k_vec;
    k_vec.resize(m_ndims);
    for (int I=0; I<m_num_blocks; ++I) {
        for (int j=0; j<m_block_size; ++j) {
            // determine indices within block k_vec given index j
            set_index_vector(j, k_vec, m_block_shape);
            int d {0};
            m_dcts_lossy[I][j] = m_dcts[I][j];
            while(d < m_ndims) {
                if (k_vec[d] >= shape_to_keep[d]) {
                    m_dcts_lossy[I][j] = 0.0;
                    break;
                }
                ++d;
            }
        }
    }
    m_compressed = true;
}

void DataProcessor::quantize_dct(int num_bits) {
    // brief: Reduce the resolution of the data to be num_bits. Break the range from 0 to the value of the largest dct
    // coefficient (probably the DC component) and choose the appropriate step size between alphabet values (where the
    // length of the alphabet is given by 2^num_bits). Then round each coefficient of the dct to the nearest value in
    // the alphabet, and store the corresponding integer. The idea would be to then apply Huffman coding to the
    // resulting quantized dct matrix.
    assert(m_got_dct && "Need to compute dct before quantizing dct\n");

    m_dcts_quant.resize(m_num_blocks);
    for (int I=0; I<m_num_blocks; ++I) {
        m_dcts_quant[I] = new int[m_block_size];
    }
    m_step_size.resize(m_num_blocks);

    for (int I=0; I<m_num_blocks; ++I) {
        // 1. determine dct coefficient with largest magnitude in this block.
        double max {0};
        for (int j=0; j<m_block_size; ++j) {
            if (std::abs(m_dcts[I][j]) > max) max = std::abs(m_dcts[I][j]);
        }
        // 2. compute step size
        int num_steps {pow_int(2, num_bits-1) - 1};
        m_step_size[I] = max/num_steps;
        // 3. go through all coefficients in this block and round to nearest alphabet value.
        for (int j=0; j<m_block_size; ++j) {
            m_dcts_quant[I][j] = round(m_dcts[I][j]/m_step_size[I]);
        }

        // debugging
        // if (I == 0) {
        //     std::cout << "exact dct:\n";
        //     for (int j=0; j<m_block_size; ++j) {
        //         std::cout << m_dcts[I][j] << " ";
        //     }
        //     std::cout << "\n";
        //     std::cout << "m_step_size: " << m_step_size[I] << "\n";
        //     std::cout << "quantized dct:\n";
        //     for (int j=0; j<m_block_size; ++j) {
        //         std::cout << m_dcts_quant[I][j] << " ";
        //     }
        //     std::cout << "\n";
        //     std::cout << "dequantized dct:\n";
        //     for (int j=0; j<m_block_size; ++j) {
        //         std::cout << m_step_size[I]*m_dcts_quant[I][j] << " ";
        //     }
        //     std::cout << "\n";
        // }
    }
    m_num_bits = num_bits;
    m_quantized = true;
}

void count_symbols(std::list<Leaf> &queue, int symbol) {
    int max {static_cast<int>(queue.size())};
    if (max == 0) {
        queue.push_back(Leaf(symbol, 1.0));
    } else {
        --max;
        int min {0}, centre {0}, old_centre, queued_symbol;
        double freq;
        std::list<Leaf>::iterator it_c {queue.begin()};
        while(true) {
            old_centre = centre;
            centre = (max + min)/2;
            if (centre-old_centre) advance(it_c, centre-old_centre);
            queued_symbol = it_c->get_symbol();
            if (queued_symbol < symbol) {
                min = centre+1;
            } else if (queued_symbol > symbol) {
                max = centre-1;
            } else {
                freq = it_c->get_freq() + 1.0;
                it_c->set_freq(freq);
                break;
            }
            if (min > max) {
                if (min > centre) ++it_c;
                queue.emplace(it_c, symbol, 1.0);
                break;
            }
        }
    }
}

void merge(const std::vector<Leaf> &input, std::vector<Leaf> &merged_output,
           const int begin, const int centre, const int end, const LeafAttribute att) {
    int i {begin}, j {centre};
    bool condition;
    for (int k=begin; k<end; ++k) {
        switch (att) {
        case LeafAttribute::SYMBOL: // sort symbols in increasing order
            condition = (i < centre) && (j >= end || input[i].get_symbol() <= input[j].get_symbol());
            break;
        case LeafAttribute::FREQ: // sort frequencies in decreasing order
            condition = (i < centre) && (j >= end || input[i].get_freq() >= input[j].get_freq());
            break;
        default:
            throw std::runtime_error("invalid LeafAttribute\n");
            break;
        }
        if (condition) {
            merged_output[k] = input[i];
            ++i;
        } else {
            merged_output[k] = input[j];
            ++j;
        }
    }
}

void split_merge(std::vector<Leaf> &leaves, std::vector<Leaf> &leaves_copy, const int begin, const int end, const LeafAttribute att) {
    if (end - begin < 2) return;
    int centre = (begin+end)/2;
    split_merge(leaves_copy, leaves, begin, centre, att);
    split_merge(leaves_copy, leaves, centre, end, att);
    merge(leaves_copy, leaves, begin, centre, end, att);
}

void sort_leaves(std::vector<Leaf> &leaves, const LeafAttribute att) {
    std::vector<Leaf> leaves_copy {leaves};
    int begin {0}, end {static_cast<int>(leaves.size())};
    split_merge(leaves, leaves_copy, begin, end, att);
}

void search_code(const int symbol, const std::vector<Leaf> &code_list, std::vector<unsigned char> &code) {
    int min {0}, max {static_cast<int>(code_list.size()) - 1};
    int centre;
    while(true) {
        if (min > max) throw std::runtime_error("search_code failed to find symbol\n");
        centre = (max + min)/2;
        if (code_list[centre].get_symbol() < symbol) {
            min = centre+1;
        } else if (code_list[centre].get_symbol() > symbol) {
            max = centre-1;
        } else {
            code_list[centre].get_code(code);
            break;
        }
    }
}

void DataProcessor::encode_dct() {
    assert(m_quantized && "Need to quantize dct before encoding dct\n");
    std::list<Leaf> queue1_list;
    std::vector<Leaf> queue1;
    std::list<Branch> queue2;
    m_dcts_encoded.resize(m_num_blocks);
    const int ROOT_DEPTH {2};

    // compress each block separately
    // compression steps:
    // 1. Generate list of symbols in the block and their frequencies (these should be of type Leaf)
    std::cout << "counting symbols";
    float frac_done {0.0};
    queue1_list.clear();
    for (int I=0; I<m_num_blocks; ++I) {
        for (int j=0; j<m_block_size; ++j) {
            count_symbols(queue1_list, m_dcts_quant[I][j]);
        }
        frac_done += 60.0/m_num_blocks;
        if (frac_done > 1.0) {
            std::cout << "." << std::flush;
            frac_done -= 1.0;
        }
    }
    std::cout << "\n";

    // debugging
    // std::cout << "Step 1 done\n";
    // std::list<Leaf>::iterator iterator {queue1_list.begin()};
    // int list_size {static_cast<int>(queue1_list.size())};
    // std::cout << "list size: " << list_size << "\n";
    // for (int iii=0; iii < list_size; ++iii) {
    //     std::cout << "symbol: " << iterator->get_symbol() << "\t";
    //     std::cout << "frequency: " << iterator->get_freq() << "\n";
    //     ++iterator;
    // }

    // 2. Sort the list in order of decreasing frequencies (so highest priority terms are at the back of the list);
    //    this is the first queue (the first queue is of type Leaf, and the second is of type Branch)
    std::cout << "sorting queue...\n";

    queue1.clear();
    int num_symbols {static_cast<int>(queue1_list.size())};
    queue1.resize(num_symbols);
    auto it = queue1_list.begin();
    for (int s=0; s<num_symbols; ++s) {
        queue1[s] = *it;
        ++it;
    }
    sort_leaves(queue1, LeafAttribute::FREQ);

    // debugging
    // std::cout << "Step 2 done\n";
    // std::cout << "num_symbols: " << num_symbols << "\n";
    // std::cout << "num_symbols = " << num_symbols << "\n";
    // for (int iii=0; iii < num_symbols; ++iii) {
    //     std::cout << "symbol: " << queue1[iii].get_symbol() << "\t";
    //     std::cout << "frequency: " << queue1[iii].get_freq() << "\n";
    // }

    // 3. Generate encoding tree:
    //    (i) Remove two lowest frequency elements from the first and/or second queue, and place them in the tree
    //        array variable. Then add a Branch node to the back of the second queue that points to these two
    //        elements (which are now in the array), and whose frequency is the sum of the frequencies of the these
    //        two elements.
    //    (ii) Repeat step (i) until there is only one element left in the queue; this is the root of the tree

    std::cout << "generating encoding tree...\n";

    queue2.clear();
    m_tree.leaves.clear(); m_tree.branches.clear();
    std::vector<Node *> branch_nodes;
    branch_nodes.resize(2); // binary tree
    double branch_freq;
    for (int s=0; s<num_symbols-1; ++s) {
        branch_freq = 0.0;
        for (int nd=0; nd<2; ++nd) {
            if (queue2.empty() || (!(queue1.empty()) && queue1.back().get_freq() <= queue2.back().get_freq())) {
                m_tree.leaves.push_back(queue1.back());
                queue1.pop_back();
                branch_freq += m_tree.leaves.back().get_freq();
                branch_nodes[nd] = &(m_tree.leaves.back());
                // } else if ( !(queue1.empty()) ) {
                //     if (queue1.back().get_freq() <= queue2.back().get_freq()) {
                //         tree_it->leaves.push_back(queue1.back());
                //         queue1.pop_back();
                //         branch_freq += tree_it->leaves.back().get_freq();
                //         branch_nodes[nd] = &(tree_it->leaves.back());
                //     } else {
                //         tree_it->branches.push_back(queue2.back());
                //         queue2.pop_back();
                //         branch_freq += tree_it->branches.back().get_freq();
                //         branch_nodes[nd] = &(tree_it->branches.back());
                //     }
            } else {
                m_tree.branches.push_back(queue2.back());
                queue2.pop_back();
                branch_freq += m_tree.branches.back().get_freq();
                branch_nodes[nd] = &(m_tree.branches.back());
            }
        }
        queue2.push_front( Branch(branch_nodes[0] ,branch_nodes[1] ,branch_freq) );
        // Note: not assinging parent liks to children at this point since the parent is in the queue and therefore
        // will be copied to the tree later (and will therefore have a different final address). So makes more sense
        // to assign parents when we walk through the tree later.
    }
    m_tree.root = queue2.back();
    queue2.pop_back();
    assert(queue1.empty() && queue2.empty() && "queues not empty after tree generation\n");

    // debugging
    // std::cout << "Step 3 done\n";
    // std::cout << "Checking tree integrity\n";
    // std::cout << "Number of leaves: " << tree_it->leaves.size() << "\n";
    // std::cout << "Number of branches: " << tree_it->branches.size() << "\n";
    // for (Branch branch : tree_it->branches) {
    //     std::cout << "branch type: " << branch.get_type() << "\n";
    //     std::cout << "child zero type: " << (branch.get_child(Child::ZERO))->get_type() << "\n";
    //     std::cout << "child one type: " << (branch.get_child(Child::ONE))->get_type() << "\n";
    // }

    // 4. Systematically go through the encoding tree to determine and store an index of symbols and their
    //    corresponding encodings; you can then use this to faster encode the data.
    // Detailed steps:

    // 1. Start at the root of the tree; this is a Branch class instance.

    // 2. Acces m_child0 of the root and assign bit 0 to the first bit of the code (i.e. don't do anything in this
    //    case). Each time you acces a child node, also assign the parent to the child since this hasn't been done
    //    yet, and will be used to walk back up the tree at a later step.

    // 3. Check the type of the Node pointed to by calling the member function get_type() of the node. If the Node
    //    is a branch, access m_child0 and repeat. If the Node is a Leaf, the code is complete; copy the code to the
    //    index as well as the symbol (just copy the Leaf to the index, and assign the code to the Leaf as well).

    // 4. Every time you reach a Leaf Node and copy the symbol and code to the index, then you need to climb back up
    //    the tree. Do this by accessing the parent of the current node.

    std::cout << "generating index of codes...\n";

    std::vector<unsigned char> code;
    Node *child {&(m_tree.root)}, *parent {nullptr};
    int depth {ROOT_DEPTH}, max_depth {0}, iBit, iByte; // setting the root to depth 2 so that we can use the first
    // three bits to encode the number of used bits in the last
    // byte
    bool climb_tree {true};
    while(true) {
        // debugging
        // std::cout << "Step 4, depth: " << depth << "\n";
        // std::cout << "Step 4, max_depth: " << max_depth << "\n";

        if (child->get_type() == 'L') {// this is a leaf node
            // debugging
            // std::cout << "Leaf Node reached\n";

            // set the first three bits of the code as the number of used bits in the last byte
            iBit = depth%8;
            int expon {2}, val;
            while(expon >= 0) {
                val = pow_int(2, expon);
                if (iBit >= val) {
                    code[0] |= BITS[2-expon];
                    iBit -= val;
                } else {
                    code[0] &= ~BITS[2-expon];
                }
                --expon;
            }

            // debugging
            // std::cout << "bits in last byte recorded\n";

            Leaf leaf {child->get_symbol(), child->get_freq()};
            leaf.set_code(code);
            leaf.set_parent(child->get_parent());
            m_codes.push_back(leaf);
            climb_tree = false;
            child = parent;
            parent = child->get_parent();
            --depth;
            if (max_depth > depth+1+8) {
                code.pop_back();
                max_depth -= 8;
            }

            // debugging
            // std::cout << "Leaf Node recorded\n";
        } else if (child->get_type() == 'B') {// this is a branch node
            // debugging
            // std::cout << "Branch Node reached\n";

            // branch = *child;
            if (climb_tree) {
                // debugging
                // std::cout << "Climbing\n";

                if (max_depth <= depth+1) {
                    code.push_back(0b00000000);
                    max_depth += 8;
                }
                parent = child;
                child = parent->get_child(Child::ZERO);
                child->set_parent(parent);
                ++depth;
            } else {
                // debugging
                // std::cout << "Descending\n";

                // determine the value of the bit at depth+1
                iByte = (depth+1)/8;
                iBit  = (depth+1)%8;
                if (code[iByte] & BITS[iBit]) {// bit is 1
                    // debugging
                    // std::cout << "Bit was a 1\n";

                    // check if we are back at the root, in which case we're done
                    if (depth == ROOT_DEPTH) break;
                    // set bit at depth+1 back to 0
                    code[iByte] &= ~BITS[iBit];
                    // keep coming down the tree
                    child = parent;
                    parent = child->get_parent();
                    --depth;
                    if (max_depth > depth+1+8) {
                        code.pop_back();
                        max_depth -= 8;
                    }
                } else {// bit is 0
                        // debugging
                        // std::cout << "Bit was a 0\n";

                        // set bit at depth+1 to 1
                    code[iByte] |= BITS[iBit];
                    // start climbing the tree again up the other branch
                    climb_tree = true;
                    parent = child;
                    child = parent->get_child(Child::ONE);
                    child->set_parent(parent);
                    ++depth;
                }
            }
        } else {
            throw std::runtime_error("tree Node is neither a Leaf nor a Branch\n");
        }
    }

    // debugging
    // std::cout << "Step 4 done\n";
    // std::cout << "code_list size: " << m_codes[I].size() << "\n";
    // std::cout << "code_list:\n";
    // for (auto &leaf : m_codes[I]) {
    //     std::cout << "symbol: " << leaf.get_symbol() << ";\t";
    //     std::cout << "code: ";
    //     std::vector<unsigned char> leaf_code;
    //     leaf.get_code(leaf_code);
    //     for (int iByte=0; iByte < static_cast<int>(leaf_code.size()); ++iByte) {
    //         for (int iBit=0; iBit < 8; ++iBit) {
    //             if (leaf_code[iByte] & BITS[iBit]) {
    //                 std::cout << 1;
    //             } else {
    //                 std::cout << 0;
    //             }
    //         }
    //     }
    //     std::cout << "\n";
    // }

    // 5. Now sort the index according to increasing order of symbols.
    sort_leaves(m_codes, LeafAttribute::SYMBOL);

    std::cout << "sorting index...\n";

    // debugging
    // std::cout << "Step 5 done\n";

    // 6. Encode data. Go through all elements in the block and append the encoded symbols to a bit string array
    // (should use bit flags for the encoding to get optimal compression). At this step you can use binary search on
    // the index to get decent perfomance. That's it, encoding is done! To decode you will just need to step through
    // the encoded bit string and the tree array, then multiply the resulting dct array by the step size, and take
    // the inverse transform.

    std::cout << "encoding data...\n";

    for (int I=0; I<m_num_blocks; ++I) {
        int iByteWrite {0}, iBitWrite {0};
        m_dcts_encoded[I].push_back(0b00000000);
        for (int j=0; j<m_block_size; ++j) {
            // 1. find code in m_codes[I]
            code.clear();
            search_code(m_dcts_quant[I][j], m_codes, code);
            // 2. write code to m_dcts_encoded[I]
            int num_bytes {static_cast<int>(code.size())};
            int num_bits_last_byte {1};
            for (int iByteRead=0; iByteRead<num_bytes; ++iByteRead) {
                for (int iBitRead=0; iBitRead<8; ++iBitRead) {
                    if (iByteRead == 0 && iBitRead <= ROOT_DEPTH) {
                        if (code[iByteRead] & BITS[iBitRead]) num_bits_last_byte += pow_int(2, (2-iBitRead));
                    } else if ((num_bytes == 1 && iBitRead > ROOT_DEPTH && iBitRead >= num_bits_last_byte) ||
                               (num_bytes > 1 && iByteRead == num_bytes-1 && iBitRead >= num_bits_last_byte)) {
                        break;
                    } else {
                        if (code[iByteRead] & BITS[iBitRead]) {
                            m_dcts_encoded[I][iByteWrite] |= BITS[iBitWrite];
                        }
                        ++iBitWrite;
                        if (iBitWrite >= 8) {
                            m_dcts_encoded[I].push_back(0b00000000);
                            ++iByteWrite;
                            iBitWrite -= 8;
                        }
                    }
                }
            }
        }
    }

    std::cout << "encoding done.\n";

    // debugging
    // std::cout << "Step 6 done\n";
    // std::cout << "block symbols:\n";
    // for (int j=0; j<m_block_size; ++j) {
    //     std::cout << m_dcts_quant[I][j] << "\t";
    // }
    // std::cout << "\n";
    // std::cout << "encoded block:\n";
    // int count {0};
    // for (const auto &byte : m_dcts_encoded[1]) {
    //     for (int iBit=0; iBit<8; ++iBit) {
    //         if (byte & BITS[iBit]) {
    //             std::cout << 1;
    //         } else {
    //             std::cout << 0;
    //         }
    //     }
    //     ++count;
    //     if (!(count%10)) std::cout << "\n";
    // }
    // std::cout << "\n";

    m_encoded = true;
}

void DataProcessor::decode_dct_block(const int I /* block index */, double *dct_decoded_out) {
    int iByte {0}, iBit {0};
    for (int j=0; j<m_block_size; ++j) {
        const Node *node {&(m_tree.root)};
        while(true) {
            if (m_dcts_encoded[I][iByte] & BITS[iBit]) {
                node = node->get_child(Child::ONE);
            } else {
                node = node->get_child(Child::ZERO);
            }
            ++iBit;
            if (iBit >= 8) {
                ++iByte;
                iBit -= 8;
            }
            if (node->get_type() == 'L') break;
        }
        dct_decoded_out[j] = (node->get_symbol())*m_step_size[I];
    }
}

void DataProcessor::decode_dct() {
    assert(m_encoded && "Cannot decode dct, it hasn't been encoded\n");

    m_dcts_decoded.resize(m_num_blocks);
    for (int I=0; I<m_num_blocks; ++I) {
        m_dcts_decoded[I] = new double[m_block_size];
    }

    for (int I=0; I<m_num_blocks; ++I) {
        decode_dct_block(I, m_dcts_decoded[I]);
    }

    // debugging
    // std::cout << "m_dcts_decoded[0]:\n";
    // for (int j=0; j<m_block_size; ++j) {
    //     std::cout << m_dcts_decoded[0][j] << " ";
    //     if (!((j+1)%40)) std::cout << "\n";
    // }

    m_decoded = true;
}

void DataProcessor::compute_idct_block(const double *dct_in, double *idct_out) {
    fftw_plan idct_plan;
    int *nnn = new int[m_ndims];
    fftw_r2r_kind *kind = new fftw_r2r_kind[m_ndims];
    for (int d=0; d<m_ndims; ++d) {
        nnn[d] = m_block_shape[d];
        kind[d] = FFTW_REDFT01;
    }
    double *dct = new double[m_block_size];
    idct_plan = fftw_plan_r2r(m_ndims, nnn, dct, idct_out, kind, FFTW_MEASURE);
    for (int j=0; j<m_block_size; ++j) {
        dct[j] = dct_in[j];
    }
    fftw_execute(idct_plan);
    delete[] dct;
    delete[] nnn;
    delete[] kind;
}

void DataProcessor::compute_idct(bool compressed) {
    if (compressed) {
        assert(m_decoded && "Need to decode dct before taking idct\n");
    } else {
        assert(m_got_dct && "Need to compute dct before taking idct\n");
    }
    std::vector<int> l_vec, k_vec, n_vec;
    l_vec.resize(m_ndims); k_vec.resize(m_ndims); n_vec.resize(m_ndims);
    double *idct_I = new double[m_block_size];
    int i;
    m_approx.resize(m_size);
    double rmse {0.0};
    for (int I=0; I<m_num_blocks; ++I) {
        if (compressed) {
            compute_idct_block(m_dcts_decoded[I], idct_I);
        } else {
            compute_idct_block(m_dcts[I], idct_I);
        }

        set_index_vector(I, l_vec, m_shape_blocks);
        // assign approximate data to m_approx
        for (int j=0; j<m_block_size; ++j) {
            // find index i of m_dataset corresponding to (I, j)
            set_index_vector(j, k_vec, m_block_shape);
            for (int d=0; d<m_ndims; ++d) {
                // uncomment line below for non-overlapping blocks
                // n_vec[d] = l_vec[d]*m_block_shape[d] + k_vec[d];
                n_vec[d] = l_vec[d]*(m_block_shape[d]-1) + k_vec[d];
            }
            i = get_index(n_vec, m_shape);

            // assign inverse dct value to reconstructed data
            m_approx[i] = idct_I[j];
        }

        // for overlapping blocks we need to compute the rms-error now since points on the edges of blocks can be
        // obtained from more than one block (so some elements of m_approx will be rewritten by the next block)
        if (m_got_data) {
            // compute reconstruction error (RMSE)
            for (int j=0; j<m_block_size; ++j) {
                // find index i of m_dataset corresponding to (I, j)
                set_index_vector(j, k_vec, m_block_shape);
                for (int d=0; d<m_ndims; ++d) {
                    // uncomment line below for non-overlapping blocks
                    // n_vec[d] = l_vec[d]*m_block_shape[d] + k_vec[d];
                    n_vec[d] = l_vec[d]*(m_block_shape[d]-1) + k_vec[d];
                }
                i = get_index(n_vec, m_shape);
                double diff {m_dataset[i] - m_approx[i]};
                rmse += diff*diff;
            }
        }
    }
    delete[] idct_I;

    if (m_got_data) {
        // finish computing reconstruction error (RMSE)
        rmse /= m_num_blocks*m_block_size; // not the same as dividing by m_size for overlapping blocks
        rmse = sqrt(rmse);
        if (compressed) {
            std::cout << "Compressed ";
        } else {
            int uncompressed_bytes {8*m_size};
            int compressed_bytes {0};
            compressed_bytes += ((m_num_bits-1)/8 + 1)*static_cast<int>(m_tree.leaves.size());
            // this assumes we only keep the leaf symbol attributes since we don't need the others for decoding, and we
            // store the symbols optimally in terms of bytes
            std::vector<unsigned char> code_out;
            for (const auto &leaf : m_tree.leaves) {
                leaf.get_code(code_out);

                // Plus 1 for extra_bytes (see write_data()); actually the way the hdf5 files are written is less
                // efficient since it writes the length of the longest code for all codes, but it may be possible to
                // do better in general.
                compressed_bytes += static_cast<int>(code_out.size()) + 1;
            }
            // this assumes we only keep children attributes for branches since this is all we need for
            // decoding. Also could assume a 64 bit architecture, so pointers are 8 bytes, but for the moment not
            // considering pointers (whether you do so depends on what you want to know; compression ratio to file
            // or compression ratio when storing the tree in memory)
            for (int I=0; I<m_num_blocks; ++I) {
                compressed_bytes += static_cast<int>(m_dcts_encoded[I].size());
                compressed_bytes += 8; // for step size data (double)
            }
            std::cout << "Compression ratio: " << static_cast<double>(uncompressed_bytes)/compressed_bytes << "\n";
            std::cout << "Uncompressed ";
        }
        std::cout << "RMS reconstruction error: " << rmse << "\n";
    }

    m_approximated = true;
}

void to_bin(int int_in, unsigned char *bin_out, const int MAX_BYTES) {
    assert(int_in > 0 && "Input integer is negative.\n");
    int val;
    int expon {8*MAX_BYTES - 1};
    for (int iByte=0; iByte<MAX_BYTES; ++iByte) {
        for (int iBit=0; iBit<8; ++iBit) {
            val = pow_int(2, expon);
            if (int_in >= val) { // set bit to 1
                bin_out[iByte] |= BITS[iBit];
                int_in -= val;
            } else { // set bit to 0
                bin_out[iByte] &= ~BITS[iBit];
            }
            --expon;
        }
    }
}

void to_bin_signed(const int int_in, unsigned char *bin_out, const int MAX_BYTES) {
    int int_pos {std::abs(int_in)};
    int val;
    int expon {8*MAX_BYTES - 2};
    for (int iByte=0; iByte<MAX_BYTES; ++iByte) {
        for (int iBit=0; iBit<8; ++iBit) {
            if (iByte || iBit) {
                val = pow_int(2, expon);
                if (int_pos >= val) { // set bit to 1
                    bin_out[iByte] |= BITS[iBit];
                    int_pos -= val;
                } else { // set bit to 0
                    bin_out[iByte] &= ~BITS[iBit];
                }
                --expon;
            } else {
                if (int_in < 0) { // set bit to 1
                    bin_out[iByte] |= BITS[iBit];
                } else { // set bit to 0
                    bin_out[iByte] &= ~BITS[iBit];
                }
            }
        }
    }
}

int to_int(const unsigned char *bin_in, const int MAX_BYTES) {
    int int_out {0};
    int expon {8*MAX_BYTES - 1};
    for (int iByte=0; iByte<MAX_BYTES; ++iByte) {
        for (int iBit=0; iBit<8; ++iBit) {
            if (bin_in[iByte] & BITS[iBit]) {
                int_out += pow_int(2, expon);
            }
            --expon;
        }
    }
    return int_out;
}

int to_int_signed(const unsigned char *bin_in, const int MAX_BYTES) {
    int int_out {0};
    int sign {1};
    int expon {8*MAX_BYTES - 2};
    for (int iByte=0; iByte<MAX_BYTES; ++iByte) {
        for (int iBit=0; iBit<8; ++iBit) {
            if (iByte || iBit) {
                if (bin_in[iByte] & BITS[iBit]) {
                    int_out += pow_int(2, expon);
                }
                --expon;
            } else {
                if (bin_in[iByte] & BITS[iBit]) {
                    sign = -1;
                }
            }
        }
    }
    return sign*int_out;
}

// void DataProcessor::write_data(const std::string FILENAME) {
//     // Write encoded data to an hdf5 file.

//     // Overview

//     // Generate an hdf5 file with two groups: symbols, and codes. Each group contains a number m_num_blocks of datasets

//     // The datasets in symbols are rank 2 arrays containing the integer symbols (encoded using bit flags into unsigned
//     // char for efficiency; the second dimension is the number of bytes).

//     // The datasets in codes are not optimally stored, but doing so would take a lot of coding effort for a small
//     // benefit. As for the symbols group, the codes group contains a number m_num_blocks of datasets. Each dataset is a
//     // rank 2 array where the first dimension is the number of codes/symbols and the second is the number of bytes in
//     // the longest code + the number of bytes needed to store the number of bytes in the longest code as a binary digit
//     // (unlikely to be more than 1 byte since that would allow up to 256 bytes for the longest code which would only
//     // result from an extremely large alphabet of symbols; might be impossible with the current code since the symbols
//     // are currently stored as type int, meaning the largest alphabet would have 2^32 symbols, but safer to keep things
//     // general).

//     // An additional dataset (not belonging to any group) is a rank 1 array of doubles of size m_num_blocks containing
//     // the step sizes for each block. Other datasets are shape, and block_shape which record the shape of the dataset
//     // and the shape of the blocks. extra_code_bytes is an array used to record the extra bytes used for storing the
//     // codes of each block.

//     // The encoded dct for each block is written to a corresponding dataset in the /data group

//     assert(m_encoded && "dct must be encoded before writing to file\n");
//     const H5std_string FILE_NAME {FILENAME};
//     const H5std_string DATASET_NAME_SHAPE {"shape"};
//     const H5std_string DATASET_NAME_BLOCK_SHAPE {"block_shape"};
//     const H5std_string DATASET_NAME_STEPS {"step_sizes"};
//     const H5std_string DATASET_NAME_EXTRA_BYTES {"extra_code_bytes"};
//     const H5std_string DATASET_NAME_SYMBOLS {"symbols"};
//     const H5std_string DATASET_NAME_CODES {"codes"};
//     const H5std_string GROUP_NAME_DATA {"/data"};
//     const int RANK_SHAPE {1}, RANK_STEPS {1}, RANK_EXTRA_BYTES {1}, RANK_SYMBOLS {2}, RANK_CODES {2}, RANK_DATA {1};
//     std::vector<H5std_string> dataset_name_data;
//     dataset_name_data.resize(m_num_blocks);
//     for (int I=0; I<m_num_blocks; ++I) {
//         dataset_name_data[I] = std::to_string(I);
//     }

//     // generate data
//     int shape_data[m_ndims];
//     int block_shape_data[m_ndims];
//     for (int d=0; d<m_ndims; ++d) {
//         shape_data[d] = m_shape[d];
//         block_shape_data[d] = m_block_shape[d];
//     }
//     double *step_size_data = new double[m_num_blocks];
//     unsigned char extra_bytes_data[1];
//     const int SYMBOL_BYTES {1+(m_num_bits-1)/8};
//     const int NUM_SYMBOLS {static_cast<int>(m_codes.size())};

//     // determine the number of bytes in the longest code
//     int max_code_bytes {0}, num_code_bytes;
//     std::vector<unsigned char> code_out;
//     for (const auto &leaf : m_codes) {
//         leaf.get_code(code_out);
//         num_code_bytes = static_cast<int>(code_out.size());
//         if (max_code_bytes < num_code_bytes) max_code_bytes = num_code_bytes;
//     }
//     // determine how many extra bytes we need
//     int bit_num {static_cast<int>(std::log2(max_code_bytes-0.5))};
//     const int EXTRA_BYTES {1+bit_num/8};
//     unsigned char extra_bytes_bin[1];
//     to_bin(EXTRA_BYTES, extra_bytes_bin, 1);
//     extra_bytes_data[0] = extra_bytes_bin[0];
//     const int CODE_BYTES {max_code_bytes + EXTRA_BYTES};

//     unsigned char **symbols_data = new unsigned char*[NUM_SYMBOLS];
//     unsigned char **codes_data   = new unsigned char*[NUM_SYMBOLS];

//     // debugging
//     // std::cout << "shape: ";
//     // for (auto &dim : m_shape) std::cout << dim << " ";
//     // std::cout << "\n";
//     // std::cout << "block_shape: ";
//     // for (auto &dim : m_block_shape) std::cout << dim << " ";
//     // std::cout << "\n";
//     // std::cout << "step sizes:\n";
//     // for (int I=0; I<m_num_blocks; ++I) {
//     //     std::cout << m_step_size[I] << " ";
//     //     if (!(I%5)) std::cout << "\n";
//     // }
//     // std::cout << "\n";
//     // std::cout << "m_codes:\n";
//     // std::cout << "symbols and codes:\n";
//     // std::vector<unsigned char> code_db;
//     // for (auto &leaf : m_codes) {
//     //     std::cout << leaf.get_symbol() << ": ";
//     //     leaf.get_code(code_db);
//     //     const int num_bytes {static_cast<int>(code_db.size())};
//     //     for (int iByte=0; iByte<num_bytes; ++iByte) {
//     //         std::cout << code_db[iByte] << " ";
//     //     }
//     //     std::cout << "\n";
//     // }
//     // std::cout << "m_dcts_encoded[0]:\n";
//     // int count {0};
//     // for (auto &byte : m_dcts_encoded[0]) {
//     //     for (int iBit=0; iBit<8; ++iBit) {
//     //         if (byte & BITS[iBit]) {
//     //             std::cout << "1";
//     //         } else {
//     //             std::cout << "0";
//     //         }
//     //     }
//     //     ++count;
//     //     if (!(count%10)) std::cout << "\n";
//     // }
//     // std::cout << "\n";

//     int iSymbol {0}, symbol;
//     std::vector<unsigned char> code;
//     for (const auto &leaf : m_codes) {
//         // generate symbols data
//         symbols_data[iSymbol] = new unsigned char[SYMBOL_BYTES];
//         symbol = leaf.get_symbol();
//         to_bin_signed(symbol, symbols_data[iSymbol], SYMBOL_BYTES);
//         // generate codes data
//         codes_data[iSymbol] = new unsigned char[max_code_bytes+EXTRA_BYTES];
//         leaf.get_code(code);
//         // write the number of bytes of this code into the first "extra bytes"
//         num_code_bytes = static_cast<int>(code.size());
//         to_bin(num_code_bytes, codes_data[iSymbol], EXTRA_BYTES);
//         // write code into the remaining bytes
//         int iByte {EXTRA_BYTES};
//         for (const auto &byte : code) {
//             for (int iBit=0; iBit<8; ++iBit) {
//                 if (byte & BITS[iBit])
//                     codes_data[iSymbol][iByte] |= BITS[iBit];
//                 else
//                     codes_data[iSymbol][iByte] &= ~BITS[iBit];
//             }
//             ++iByte;
//         }
//         ++iSymbol;
//     }

//     std::vector<unsigned char *> data;
//     data.resize(m_num_blocks);
//     for (int I=0; I<m_num_blocks; ++I) {
//         step_size_data[I] = m_step_size[I];
//         // generate encoded data
//         data[I] = new unsigned char[m_dcts_encoded[I].size()];
//         for (int iByte=0; iByte<static_cast<int>(m_dcts_encoded[I].size()); ++iByte) {
//             data[I][iByte] = m_dcts_encoded[I][iByte];
//         }
//     }

//     // write dataset to file
//     try {
//         H5::Exception::dontPrint();
//         H5::H5File *file = new H5::H5File {FILE_NAME, H5F_ACC_TRUNC};

//         hsize_t dims_shape[RANK_SHAPE];
//         dims_shape[0] = m_ndims;
//         H5::DataSpace dataspace_shape {RANK_SHAPE, dims_shape};
//         H5::IntType datatype_int {H5::PredType::NATIVE_INT};
//         H5::DataSet dataset_shape { file->createDataSet(DATASET_NAME_SHAPE, datatype_int, dataspace_shape) };
//         dataset_shape.write(shape_data, H5::PredType::NATIVE_INT);

//         H5::DataSpace dataspace_block_shape {RANK_SHAPE, dims_shape};
//         H5::DataSet dataset_block_shape { file->createDataSet(DATASET_NAME_BLOCK_SHAPE, datatype_int, dataspace_block_shape) };
//         dataset_block_shape.write(block_shape_data, H5::PredType::NATIVE_INT);

//         hsize_t dims_steps[RANK_STEPS];
//         dims_steps[0] = m_num_blocks;
//         H5::DataSpace *dataspace_steps = new H5::DataSpace {RANK_STEPS, dims_steps};
//         H5::FloatType datatype_double(H5::PredType::NATIVE_DOUBLE);
//         H5::DataSet *dataset_steps = new H5::DataSet{ file->createDataSet(DATASET_NAME_STEPS, datatype_double, *dataspace_steps) };
//         dataset_steps->write(step_size_data, H5::PredType::NATIVE_DOUBLE);

//         delete dataspace_steps;
//         delete dataset_steps;

//         delete[] step_size_data;

//         // record extra code bytes; one byte is more than enough to store the integer number of extra bytes (there will
//         // probably never be more than 1 extra byte let alone 256!)
//         hsize_t dims_extra_bytes[RANK_EXTRA_BYTES];
//         dims_extra_bytes[0] = 1;
//         H5::DataSpace *dataspace_extra_bytes = new H5::DataSpace {RANK_EXTRA_BYTES, dims_extra_bytes};
//         H5::IntType datatype_uchar(H5::PredType::NATIVE_UCHAR);
//         H5::DataSet *dataset_extra_bytes = new H5::DataSet{ file->createDataSet(DATASET_NAME_EXTRA_BYTES,
//                                                                                 datatype_uchar, *dataspace_extra_bytes) };
//         dataset_extra_bytes->write(extra_bytes_data, H5::PredType::NATIVE_UCHAR);

//         delete dataspace_extra_bytes;
//         delete dataset_extra_bytes;

//         hsize_t dims_symbols[RANK_SYMBOLS];
//         dims_symbols[0] = NUM_SYMBOLS;
//         dims_symbols[1] = SYMBOL_BYTES;
//         H5::DataSpace *dataspace_symbols = new H5::DataSpace {RANK_SYMBOLS, dims_symbols};
//         H5::DataSet *dataset_symbols = new H5::DataSet{ file->createDataSet(DATASET_NAME_SYMBOLS, datatype_uchar, *dataspace_symbols) };

//         unsigned char *symbols_data_1d = new unsigned char[NUM_SYMBOLS*SYMBOL_BYTES];
//         for (int iSymbol=0; iSymbol<NUM_SYMBOLS; ++iSymbol) {
//             for (int iByte=0; iByte<SYMBOL_BYTES; ++iByte) {
//                 symbols_data_1d[iSymbol*SYMBOL_BYTES + iByte] = symbols_data[iSymbol][iByte];
//             }
//         }

//         dataset_symbols->write(symbols_data_1d, H5::PredType::NATIVE_UCHAR);

//         delete dataspace_symbols;
//         delete dataset_symbols;

//         delete[] symbols_data_1d;

//         hsize_t dims_codes[RANK_CODES];
//         dims_codes[0] = NUM_SYMBOLS;
//         dims_codes[1] = CODE_BYTES;
//         H5::DataSpace *dataspace_codes = new H5::DataSpace {RANK_CODES, dims_codes};
//         H5::DataSet *dataset_codes = new H5::DataSet{ file->createDataSet(DATASET_NAME_CODES, datatype_uchar, *dataspace_codes) };

//         unsigned char *codes_data_1d = new unsigned char[NUM_SYMBOLS*CODE_BYTES];
//         for (int iSymbol=0; iSymbol<NUM_SYMBOLS; ++iSymbol) {
//             for (int iByte=0; iByte<CODE_BYTES; ++iByte) {
//                 codes_data_1d[iSymbol*CODE_BYTES + iByte] = codes_data[iSymbol][iByte];
//             }
//         }

//         dataset_codes->write(codes_data_1d, H5::PredType::NATIVE_UCHAR);

//         delete dataspace_codes;
//         delete dataset_codes;

//         delete[] codes_data_1d;

//         for (int iSymbol=0; iSymbol<NUM_SYMBOLS; ++iSymbol) {
//             delete[] symbols_data[iSymbol];
//             delete[] codes_data[iSymbol];
//         }
//         delete[] symbols_data;
//         delete[] codes_data;

//         H5::Group *data_group = new H5::Group { file->createGroup(GROUP_NAME_DATA) };
//         for (int I=0; I<m_num_blocks; ++I) {
//             // write the encoded dct to file
//             hsize_t dims_data[RANK_DATA];
//             dims_data[0] = static_cast<int>(m_dcts_encoded[I].size());
//             H5::DataSpace *dataspace = new H5::DataSpace {RANK_DATA, dims_data};
//             H5::DataSet *dataset = new H5::DataSet{ file->createDataSet(GROUP_NAME_DATA+"/"+dataset_name_data[I],
//                                                                         datatype_uchar, *dataspace)};
//             dataset->write(data[I], H5::PredType::NATIVE_UCHAR);

//             delete dataspace;
//             delete dataset;
//             delete[] data[I];
//         }
//         delete data_group;
//         delete file;
//     }
//     catch( H5::FileIException error ) {
//         error.printErrorStack();
//     }
//     catch( H5::DataSetIException error ) {
//         error.printErrorStack();
//     }
//     catch( H5::DataSpaceIException error ) {
//         error.printErrorStack();
//     }
//     catch( H5::DataTypeIException error ) {
//         error.printErrorStack();
//     }
// }

void DataProcessor::assign_variables(const double *step_size_data, const int EXTRA_BYTES,
                                     const unsigned char *const *const symbols_data, const unsigned char *const *const codes_data,
                                     const std::vector<unsigned char *> &data,
                                     const int NUM_SYMBOLS, const std::vector<int> &data_bytes, const int SYMBOL_BYTES) {
    // Assign compressed data to member variables

    m_step_size.resize(m_num_blocks);
    for (int I=0; I<m_num_blocks; ++I) {
        m_step_size[I] = step_size_data[I];
    }

    // Generate binary tree of codes.

    // Notes: The list of codes contains all the information necessary to generate the tree. There are many ways to
    // do this. One simple approach is to go through the list of codes and, for each code, "walk" through the tree
    // adding in the required branch nodes as needed, and a leaf node when the last code bit is reached. Once this
    // has been done for all codes, the tree has been generated. Another approach is to sort the codes in increasing
    // order of the binary code values. Then the tree can be generated layer by layer starting from the root. First
    // the layers below the first leaf can be generated (all branch nodes), and a list of pointers to the branches
    // on the highest layer (ordered according to the binary values of the branches) is stored. Then we go through
    // the list of sorted codes and assign them to the tree to generate the next layer; if the code for the next
    // node is not a leaf, we generate a branch and also store a pointer to it in a new list of pointers (to be used
    // for generating the next layer); if the code is a leaf, we assign that leaf to the tree. Again, once we reach
    // the last code the tree has been generated. This is slightly more complex to code, but the tree generation
    // step is more efficient; here each node of the tree is accessed only once (more or less) in the tree
    // generation. However this method requires sorting the codes beforehand which adds some computational
    // complexity. It also requires more memory since we need to store a list of pointers to the branches on the
    // last layer (but this shouldn't be a problem since we're storing the entire tree in memory, and the list of
    // pointers will always be smaller than the tree, so not a big overhead). The first method is implemented here
    // since it's simpler and probably more efficient since we basically read each code once and do a few extra
    // steps as we read each code to generate the tree, whereas in the second method we need to read all the codes
    // to determine their numerical values, then sort them according to those values, and then generate the tree
    // (albeit slightly more efficiently).

    m_tree.leaves.clear(); m_tree.branches.clear();
    m_tree.root = Branch();
    for (int iSymbol=0; iSymbol<NUM_SYMBOLS; ++iSymbol) {
        // read extra bytes to get the number of code bytes
        const int NUM_CODE_BYTES = to_int(codes_data[iSymbol], EXTRA_BYTES);

        // read out remaining code bytes
        int last_bit {0};
        Node *parent {&(m_tree.root)};
        Node *child {nullptr};
        std::vector<unsigned char> code;
        code.resize(NUM_CODE_BYTES);
        for (int iByte=0; iByte<NUM_CODE_BYTES; ++iByte) {
            code[iByte] = codes_data[iSymbol][iByte+EXTRA_BYTES];
            for (int iBit=0; iBit<8; ++iBit) {
                if (iByte == 0 && iBit < 3) {// read out the first three bits
                    if (code[iByte] & BITS[iBit]) {
                        last_bit += pow_int(2, 2-iBit);
                    }
                } else if ((iByte < NUM_CODE_BYTES-1) || ((iByte == NUM_CODE_BYTES-1) && (iBit < last_bit))) {
                    Child child_bit = (code[iByte] & BITS[iBit]) ? Child::ONE : Child::ZERO;
                    if (!(parent->get_child(child_bit))) {
                        // the required child branch does not yet exist in the tree, so we need to create a new
                        // branch node and add it to the tree
                        m_tree.branches.push_back(Branch());
                        child = &(m_tree.branches.back());
                        parent->set_child(child_bit, child);
                        child->set_parent(parent);
                    }
                    parent = parent->get_child(child_bit);
                } else if ((iByte == NUM_CODE_BYTES-1) && (iBit == last_bit)) {// need to create a leaf node for this code
                    m_tree.leaves.push_back(Leaf());
                    child = &(m_tree.leaves.back());
                    Child child_bit = (code[iByte] & BITS[iBit]) ? Child::ONE : Child::ZERO;
                    parent->set_child(child_bit, child);
                    child->set_parent(parent);
                    child->set_symbol( to_int_signed(symbols_data[iSymbol], SYMBOL_BYTES) );
                    child->set_code(code);
                } else {// remaining bits are garbage
                    break;
                }
            }
        }
    }

    m_dcts_encoded.resize(m_num_blocks);
    for (int I=0; I<m_num_blocks; ++I) {
        // copy encoded data to member variable
        m_dcts_encoded[I].resize(data_bytes[I]);
        for (int iByte=0; iByte<data_bytes[I]; ++iByte) {
            m_dcts_encoded[I][iByte] = data[I][iByte];
        }
    }
}

// void DataProcessor::read_data(const std::string FILENAME) {
//     const H5std_string FILE_NAME {FILENAME};
//     const H5std_string DATASET_NAME_SHAPE {"shape"};
//     const H5std_string DATASET_NAME_BLOCK_SHAPE {"block_shape"};
//     const H5std_string DATASET_NAME_STEPS {"step_sizes"};
//     const H5std_string DATASET_NAME_EXTRA_BYTES {"extra_code_bytes"};
//     const H5std_string DATASET_NAME_SYMBOLS {"symbols"};
//     const H5std_string DATASET_NAME_CODES {"codes"};
//     const H5std_string GROUP_NAME_DATA {"/data"};
//     const int RANK_SHAPE {1}, RANK_STEPS {1}, RANK_EXTRA_BYTES {1}, RANK_SYMBOLS {2}, RANK_CODES {2}, RANK_DATA {1};

//     // read dataset from file
//     try {
//         H5::Exception::dontPrint();
//         H5::H5File *file = new H5::H5File { FILE_NAME, H5F_ACC_RDONLY };

//         H5::DataSet dataset_shape { file->openDataSet( DATASET_NAME_SHAPE ) };
//         H5::DataSpace dataspace_shape { dataset_shape.getSpace() };
//         hsize_t dims_shape[RANK_SHAPE];
//         dataspace_shape.getSimpleExtentDims(dims_shape);
//         m_ndims = static_cast<int>(dims_shape[0]);
//         H5::DataSpace memspace_shape {RANK_SHAPE, dims_shape};
//         int shape_data[m_ndims];
//         dataset_shape.read( shape_data, H5::PredType::NATIVE_INT, memspace_shape, dataspace_shape );

//         H5::DataSet dataset_block_shape { file->openDataSet( DATASET_NAME_BLOCK_SHAPE ) };
//         H5::DataSpace dataspace_block_shape { dataset_block_shape.getSpace() };
//         dataspace_block_shape.getSimpleExtentDims(dims_shape);
//         H5::DataSpace memspace_block_shape {RANK_SHAPE, dims_shape};
//         int block_shape_data[m_ndims];
//         dataset_block_shape.read( block_shape_data, H5::PredType::NATIVE_INT, memspace_block_shape, dataspace_block_shape );

//         if (m_allocated) deallocate_variables();
//         m_shape.resize(m_ndims); m_block_shape.resize(m_ndims);
//         for (int d=0; d<m_ndims; ++d) {
//             m_shape[d] = shape_data[d];
//             m_block_shape[d] = block_shape_data[d];
//         }
//         allocate_variables();

//         std::vector<H5std_string> dataset_name_data;
//         dataset_name_data.resize(m_num_blocks);
//         for (int I=0; I<m_num_blocks; ++I) {
//             dataset_name_data[I] = std::to_string(I);
//         }

//         // read step-size data
//         H5::DataSet *dataset_steps = new H5::DataSet { file->openDataSet( DATASET_NAME_STEPS ) };
//         H5::DataSpace *dataspace_steps = new H5::DataSpace { dataset_steps->getSpace() };
//         hsize_t dims_steps[RANK_STEPS];
//         dataspace_steps->getSimpleExtentDims(dims_steps);
//         assert(static_cast<int>(dims_steps[0]) == m_num_blocks && "Number of blocks in steps data doesn't match member variable.\n");
//         H5::DataSpace *memspace_steps = new H5::DataSpace {RANK_STEPS, dims_steps};
//         double *step_size_data = new double[m_num_blocks];
//         dataset_steps->read( step_size_data, H5::PredType::NATIVE_DOUBLE, *memspace_steps, *dataspace_steps );

//         delete memspace_steps;
//         delete dataspace_steps;
//         delete dataset_steps;

//         // read extra_bytes_data
//         H5::DataSet *dataset_extra_bytes = new H5::DataSet { file->openDataSet( DATASET_NAME_EXTRA_BYTES ) };
//         H5::DataSpace *dataspace_extra_bytes = new H5::DataSpace { dataset_extra_bytes->getSpace() };
//         hsize_t dims_extra_bytes[RANK_EXTRA_BYTES];
//         dataspace_extra_bytes->getSimpleExtentDims(dims_extra_bytes);
//         assert(static_cast<int>(dims_extra_bytes[0]) == 1 && "More than one value found for extra bytes data.\n");
//         H5::DataSpace *memspace_extra_bytes = new H5::DataSpace {RANK_EXTRA_BYTES, dims_extra_bytes};
//         unsigned char extra_bytes_data[1];
//         dataset_extra_bytes->read( extra_bytes_data, H5::PredType::NATIVE_UCHAR, *memspace_extra_bytes, *dataspace_extra_bytes );

//         const int EXTRA_BYTES {to_int(extra_bytes_data, 1)};

//         delete memspace_extra_bytes;
//         delete dataspace_extra_bytes;
//         delete dataset_extra_bytes;

//         // read symbols_data
//         H5::DataSet *dataset_symbols = new H5::DataSet { file->openDataSet( DATASET_NAME_SYMBOLS ) };
//         H5::DataSpace *dataspace_symbols = new H5::DataSpace { dataset_symbols->getSpace() };
//         hsize_t dims_symbols[RANK_SYMBOLS];
//         dataspace_symbols->getSimpleExtentDims(dims_symbols);
//         const int NUM_SYMBOLS {static_cast<int>(dims_symbols[0])};
//         const int SYMBOL_BYTES {static_cast<int>(dims_symbols[1])};
//         H5::DataSpace *memspace_symbols = new H5::DataSpace {RANK_SYMBOLS, dims_symbols};
//         unsigned char *symbols_data_1d = new unsigned char[NUM_SYMBOLS*SYMBOL_BYTES];
//         dataset_symbols->read( symbols_data_1d, H5::PredType::NATIVE_UCHAR, *memspace_symbols, *dataspace_symbols );

//         delete memspace_symbols;
//         delete dataspace_symbols;
//         delete dataset_symbols;

//         // read codes_data
//         H5::DataSet *dataset_codes = new H5::DataSet { file->openDataSet( DATASET_NAME_CODES ) };
//         H5::DataSpace *dataspace_codes = new H5::DataSpace { dataset_codes->getSpace() };
//         hsize_t dims_codes[RANK_CODES];
//         dataspace_codes->getSimpleExtentDims(dims_codes);
//         assert(static_cast<int>(dims_codes[0]) == NUM_SYMBOLS && "Number of codes doesn't match number of symbols\n");
//         const int CODE_BYTES = static_cast<int>(dims_codes[1]);
//         H5::DataSpace *memspace_codes = new H5::DataSpace {RANK_CODES, dims_codes};
//         unsigned char *codes_data_1d = new unsigned char[NUM_SYMBOLS*CODE_BYTES];
//         dataset_codes->read( codes_data_1d, H5::PredType::NATIVE_UCHAR, *memspace_codes, *dataspace_codes );

//         delete memspace_codes;
//         delete dataspace_codes;
//         delete dataset_codes;

//         unsigned char **symbols_data = new unsigned char*[NUM_SYMBOLS];
//         unsigned char **codes_data   = new unsigned char*[NUM_SYMBOLS];
//         for (int iSymbol=0; iSymbol<NUM_SYMBOLS; ++iSymbol) {
//             symbols_data[iSymbol] = new unsigned char[SYMBOL_BYTES];
//             for (int iByte=0; iByte<SYMBOL_BYTES; ++iByte) {
//                 symbols_data[iSymbol][iByte] = symbols_data_1d[iSymbol*SYMBOL_BYTES + iByte];
//             }
//             codes_data[iSymbol] = new unsigned char[CODE_BYTES];
//             for (int iByte=0; iByte<CODE_BYTES; ++iByte) {
//                 codes_data[iSymbol][iByte] = codes_data_1d[iSymbol*CODE_BYTES + iByte];
//             }
//         }
//         delete[] symbols_data_1d;
//         delete[] codes_data_1d;

//         // read encoded data
//         std::vector<unsigned char *> data;
//         data.resize(m_num_blocks);
//         std::vector<int> data_bytes;
//         data_bytes.resize(m_num_blocks);
//         H5::Group *data_group = new H5::Group { file->openGroup(GROUP_NAME_DATA) };
//         for (int I=0; I<m_num_blocks; ++I) {
//             H5::DataSet *dataset = new H5::DataSet { data_group->openDataSet( dataset_name_data[I] ) };
//             H5::DataSpace *dataspace = new H5::DataSpace { dataset->getSpace() };
//             hsize_t dims_data[RANK_DATA];
//             dataspace->getSimpleExtentDims(dims_data);
//             data_bytes[I] = static_cast<int>(dims_data[0]);
//             H5::DataSpace *memspace = new H5::DataSpace {RANK_DATA, dims_data};
//             data[I] = new unsigned char[data_bytes[I]];
//             dataset->read( data[I], H5::PredType::NATIVE_UCHAR, *memspace, *dataspace );

//             delete memspace;
//             delete dataspace;
//             delete dataset;
//         }
//         delete data_group;
//         delete file;

//         // assign member variables
//         assign_variables(step_size_data, EXTRA_BYTES, symbols_data,
//                          codes_data, data, NUM_SYMBOLS, data_bytes, SYMBOL_BYTES);

//         // deallocate buffers:
//         delete[] step_size_data;
//         for (int iSymbol=0; iSymbol<NUM_SYMBOLS; ++iSymbol) {
//             delete[] symbols_data[iSymbol];
//             delete[] codes_data[iSymbol];
//         }
//         delete[] symbols_data;
//         delete[] codes_data;
//         for (int I=0; I<m_num_blocks; ++I) {
//             delete[] data[I];
//         }
//     }
//     catch( H5::FileIException error ) {
//         error.printErrorStack();
//     }
//     catch( H5::DataSetIException error) {
//         error.printErrorStack();
//     }
//     catch( H5::DataSpaceIException error ) {
//         error.printErrorStack();
//     }
//     catch( H5::DataTypeIException error ) {
//         error.printErrorStack();
//     }
//     m_encoded = true;
//     m_decoded = false;
// }

void DataProcessor::set_data(const api::compressed_data &data_struct) {
    m_ndims = static_cast<int>(data_struct.shape.size());
    if (m_allocated) deallocate_variables();
    m_shape.resize(m_ndims), m_block_shape.resize(m_ndims);
    for (int d=0; d<m_ndims; ++d) {
        m_shape[d] = data_struct.shape[d];
        m_block_shape[d] = data_struct.block_shape[d];
    }
    allocate_variables();

    // copy elements of compressed_data struct to variable types to be input to existing function assign_variables()

    double *step_size_data = new double[m_num_blocks];
    for (int I=0; I<m_num_blocks; ++I) {
        step_size_data[I] = data_struct.steps[I];
    }

    const int EXTRA_BYTES {to_int(data_struct.extra_code_bytes, 1)};

    const int NUM_SYMBOLS {static_cast<int>(data_struct.symbols.size())};
    const int SYMBOL_BYTES {static_cast<int>(data_struct.symbols[0].size())};
    const int CODE_BYTES {static_cast<int>(data_struct.codes[0].size())};

    unsigned char **symbols_data = new unsigned char*[NUM_SYMBOLS];
    unsigned char **codes_data   = new unsigned char*[NUM_SYMBOLS];
    for (int iSymbol=0; iSymbol<NUM_SYMBOLS; ++iSymbol) {
        symbols_data[iSymbol] = new unsigned char[SYMBOL_BYTES];
        for (int iByte=0; iByte<SYMBOL_BYTES; ++iByte) {
            symbols_data[iSymbol][iByte] = data_struct.symbols[iSymbol][iByte];
        }
        codes_data[iSymbol] = new unsigned char[CODE_BYTES];
        for (int iByte=0; iByte<CODE_BYTES; ++iByte) {
            codes_data[iSymbol][iByte] = data_struct.codes[iSymbol][iByte];
        }
    }

    std::vector<unsigned char *> data;
    data.resize(m_num_blocks);
    std::vector<int> data_bytes;
    data_bytes.resize(m_num_blocks);
    for (int I=0; I<m_num_blocks; ++I) {
        data_bytes[I] = static_cast<int>(data_struct.data[I].size());
        data[I] = new unsigned char[data_bytes[I]];
        for (int iByte=0; iByte<data_bytes[I]; ++iByte) {
            data[I][iByte] = data_struct.data[I][iByte];
        }
    }

     // debugging
     // std::cout << "step_size_data:\n";
     // for (int I=0; I<m_num_blocks; ++I) {
         // std::cout << " " << step_size_data[I];
     // }
     // std::cout << std::endl;
     // std::cout <<  "EXTRA_BYTES: " << EXTRA_BYTES << std::endl;
     // std::cout << "symbols_data[0]:\n";
     // int iSymbol {0};
     // for (int iByte=0; iByte<SYMBOL_BYTES; ++iByte) {
         // std::cout << " " << symbols_data[iSymbol][iByte];
     // }
     // std::cout << std::endl;
     // std::cout << "codes_data[0]:\n";
     // for (int iByte=0; iByte<CODE_BYTES; ++iByte) {
         // std::cout << " " << codes_data[iSymbol][iByte];
     // }
     // std::cout << std::endl;
     // std::cout << "data[0]:\n";
     // int III {0};
     // for (int iByte=0; iByte<data_bytes[III]; ++iByte) {
         // std::cout << " " << data[III][iByte];
     // }
     // std::cout << std::endl;
     // std::cout << "NUM_SYMBOLS: " << NUM_SYMBOLS << std::endl;
     // std::cout << "data_bytes:\n";
     // for (int I=0; I<m_num_blocks; ++I) {
         // std::cout << " " << data_bytes[I];
     // }
     // std::cout << std::endl;
     // std::cout << "SYMBOL_BYTES: " << SYMBOL_BYTES << std::endl;

    // assign member variables
    assign_variables(step_size_data, EXTRA_BYTES, symbols_data,
                     codes_data, data, NUM_SYMBOLS, data_bytes, SYMBOL_BYTES);

    // deallocate buffers:
    delete[] step_size_data;
    for (int iSymbol=0; iSymbol<NUM_SYMBOLS; ++iSymbol) {
        delete[] symbols_data[iSymbol];
        delete[] codes_data[iSymbol];
    }
    delete[] symbols_data;
    delete[] codes_data;
    for (int I=0; I<m_num_blocks; ++I) {
        delete[] data[I];
    }
    m_encoded = true;
    m_decoded = false;
}

double lin_spacing(int index, int num_points, Range range) {
    return range.min + index*(range.max - range.min)/(num_points-1);
}

double poly_spacing(int index, int num_points, Range range, int power=2) {
    return range.min + pow_int(index, power)*(range.max - range.min)/pow_int(num_points-1, power);
}

double exp_spacing(int index, int num_points, Range range) {
    return range.min + exp(index)*(range.max - range.min)/exp(num_points-1);
}

void DataProcessor::set_spacings(const std::vector<int> &spacings) {
    // function mainly to write things in a way that is convenient to use with cython
    assert(m_allocated && "Dataset shape unspecified.\n");
    assert(static_cast<int>(spacings.size()) == m_ndims && "spacings size doesn't match the number of dimensions of the data\n");
    m_spacings.resize(m_ndims);
    for (int d=0; d<m_ndims; ++d) {
        m_spacings[d] = static_cast<Spacing>(spacings[d]);
    }
    m_spacings_set = true;
}

void DataProcessor::set_ranges(const std::vector<double> &mins, const std::vector<double> &maxes) {
    // function mainly to write things in a way that is convenient to use with cython
    assert(m_allocated && "Dataset shape unspecified.\n");
    assert(static_cast<int>(mins.size()) == m_ndims && "mins size doesn't match the number of dimensions of the data\n");
    assert(static_cast<int>(maxes.size()) == m_ndims && "maxes size doesn't match the number of dimensions of the data\n");
    m_ranges.resize(m_ndims);
    for (int d=0; d<m_ndims; ++d) {
        m_ranges[d].min = mins[d];
        m_ranges[d].max = maxes[d];
    }
    m_ranges_set = true;
}

void DataProcessor::set_input_points() {
    // could have passed a function pointer, but enums make things easier to use with cython

    assert(m_spacings_set && "Spacings are not set.\n");
    assert(m_ranges_set && "Ranges are not set.\n");

    m_input_points.resize(m_ndims);
    for (int d=0; d<m_ndims; ++d) {
        m_input_points[d].resize(m_shape[d]);
        for (int index=0; index<m_shape[d]; ++index) {
            switch (m_spacings[d]) {
            case Spacing::LIN:
                m_input_points[d][index] = lin_spacing(index, m_shape[d], m_ranges[d]);
                break;
            case Spacing::POLY:
                // could add functionality to do higher orders if necessary (currently quadratic by default)
                m_input_points[d][index] = poly_spacing(index, m_shape[d], m_ranges[d]);
                break;
            case Spacing::EXP:
                m_input_points[d][index] = exp_spacing(index, m_shape[d], m_ranges[d]);
                break;
            default:
                throw std::runtime_error("Invalid Spacing.\n");
            }
        }
    }
    m_points_set = true;
}

Interval search_interval(const double input_value, const std::vector<double> &interval_values) {
    Interval inter;
    int min {0}, max {static_cast<int>(interval_values.size()) - 1};
    int centre;
    while(true) {
        centre = (max + min)/2;
        if (interval_values[centre] < input_value) {
            min = centre;
        } else if (interval_values[centre] > input_value) {
            max = centre;
        } else {// very unlikely, but if this is the case, return two equal values
            inter.min = centre;
            inter.max = centre;
            return inter;
        }
        if (max - min == 1) {
            inter.min = min;
            inter.max = max;
            return inter;
        }
    }
}

double DataProcessor::approx_data_encoded(const std::vector<double> &input_point) {
    // Perform multidimensional interpolation to approximate the scalar output at the given input point.

    assert(m_encoded && "Need to encode data.\n");
    assert(m_points_set && "Need to set input space points.\n");
    assert(static_cast<int>(input_point.size()) == m_ndims && "input_point dimensions don't match those of the input space.\n");

    // 1. Determine which block to decode
    std::vector<int> l_vec; l_vec.resize(m_ndims);
    for (int d=0; d<m_ndims; ++d) {
        // Determine intervals of blocks in this dimension
        std::vector<double> block_intervals_d; block_intervals_d.resize(m_shape_blocks[d]+1);
        block_intervals_d[0] = m_input_points[d][0];
        for (int U=1; U<=m_shape_blocks[d]; ++U) {
            block_intervals_d[U] = m_input_points[d][U*(m_block_shape[d]-1)];
        }
        l_vec[d] = search_interval(input_point[d], block_intervals_d).min;
    }
    // get corresponding block index
    const int I {get_index(l_vec, m_shape_blocks)};

    // compute inverse dct
    double *dct_decoded_I = new double[m_block_size];
    decode_dct_block(I /* block index */, dct_decoded_I);
    double *idct_I = new double[m_block_size];
    compute_idct_block(dct_decoded_I, idct_I);

    // 2. Linear interpolation

    // 2.1 Determine the upper and lower bounding values for each dimension, and use this to determine the bounding data
    // points within this block.
    Interval *intervals = new Interval[m_ndims];
    std::vector<std::vector<double>> local_points; // points within block I
    local_points.resize(m_ndims);
    for (int d=0; d<m_ndims; ++d) {
        local_points[d].resize(m_block_shape[d]);
        for (int v=0; v<m_block_shape[d]; ++v) {
            local_points[d][v] = m_input_points[d][l_vec[d]*(m_block_shape[d]-1) + v];
        }
        intervals[d] = search_interval(input_point[d], local_points[d]);
    }

    // 2.2 Determine bounding points
    std::vector<std::vector<int>> bound_points;
    bound_points.resize(1);
    for (int d=0; d<m_ndims; ++d) {
        const int NUM_POINTS {static_cast<int>(bound_points.size())};
        for (int k=0; k<NUM_POINTS; ++k) {
            if (intervals[d].max != intervals[d].min) { // point doesn't lie EXACTLY at an interval so we need to interpolate
                std::vector<int> point {bound_points[k]};
                bound_points.push_back(point);
                bound_points[NUM_POINTS+k].push_back(intervals[d].max);
            }
            bound_points[k].push_back(intervals[d].min);
        }
    }

    // 2.3 Determine data values at each bounding point
    std::vector<double> bound_data;
    bound_data.resize( bound_points.size() );
    for (int p=0; p<static_cast<int>(bound_points.size()); ++p) {
        // determine block index for point
        const int j {get_index(bound_points[p], m_block_shape)};
        bound_data[p] = idct_I[j];
    }

    // 2.4 Perform linear interpolation
    for (int d=m_ndims-1; d>=0; --d) {
        // determine the fraction of the interval in this dimension
        const double LAMBDA { (input_point[d] - local_points[d][intervals[d].min]) /
                              (local_points[d][intervals[d].max] - local_points[d][intervals[d].min]) };
        const int NUM_BOUNDS {static_cast<int>(bound_data.size())/2};
        std::vector<double> new_bound_data; new_bound_data.resize(NUM_BOUNDS);
        for (int p=0; p<NUM_BOUNDS; ++p) {
            new_bound_data[p] = (1-LAMBDA)*bound_data[p] + LAMBDA*bound_data[NUM_BOUNDS+p];
        }
        bound_data = new_bound_data;
    }
    delete[] intervals;
    delete[] dct_decoded_I;
    delete[] idct_I;

    // 3. Spline interpolation (e.g. using SPLINTER -- currently requires linear spacing) could be added later if
    // desired

    return bound_data[0];
}

void merge(const std::vector<IndexPair> &input, std::vector<IndexPair> &merged_output,
           const int begin, const int centre, const int end, const IndexAtt ATT) {
    int i {begin}, j {centre};
    bool condition;
    for (int k=begin; k<end; ++k) {
        switch (ATT) {
        case IndexAtt::POINT: // sort point indices in increasing order
            condition = (i < centre) && (j >= end || input[i].iPoint <= input[j].iPoint);
            break;
        case IndexAtt::BLOCK: // sort block indices in increasing order
            condition = (i < centre) && (j >= end || input[i].iBlock <= input[j].iBlock);
            break;
        default:
            throw std::runtime_error("invalid IndexAtt\n");
            break;
        }
        if (condition) {
            merged_output[k] = input[i];
            ++i;
        } else {
            merged_output[k] = input[j];
            ++j;
        }
    }
}

void split_merge(std::vector<IndexPair> &indices, std::vector<IndexPair> &indices_copy,
                 const int begin, const int end, const IndexAtt ATT) {
    if (end - begin < 2) return;
    int centre = (begin+end)/2;
    split_merge(indices_copy, indices, begin, centre, ATT);
    split_merge(indices_copy, indices, centre, end, ATT);
    merge(indices_copy, indices, begin, centre, end, ATT);
}

void sort_indices(std::vector<IndexPair> &indices, const IndexAtt ATT) {
    // Note: should consider using templates to combine this sort function and the one for leaves.
    std::vector<IndexPair> indices_copy {indices};
    int begin {0}, end {static_cast<int>(indices.size())};
    split_merge(indices, indices_copy, begin, end, ATT);
}

void DataProcessor::approx_data_encoded(const std::vector<std::vector<double>> &input_points, std::vector<double> &output_data) {
    // Perform multidimensional interpolation to approximate the scalar output at the given input point.

    // Timer timer;

    assert(m_encoded && "Need to encode data.\n");
    assert(m_points_set && "Need to set input space points.\n");
    for (const auto &point : input_points)
        assert(static_cast<int>(point.size()) == m_ndims && "input_point dimensions don't match those of the input space.\n");

    // std::cout << "approx_data(), input points checked, time elapsed: " << timer.elapsed() << "s.\n";
    // timer.reset();

    // 1. Determine block indices
    std::vector<std::vector<double>> block_intervals;
    block_intervals.resize(m_ndims);
    for (int d=0; d<m_ndims; ++d) {
        // Determine intervals of blocks in this dimension
        block_intervals[d].resize(m_shape_blocks[d]+1);
        block_intervals[d][0] = m_input_points[d][0];
        for (int U=1; U<=m_shape_blocks[d]; ++U) {
            block_intervals[d][U] = m_input_points[d][U*(m_block_shape[d]-1)];
        }
    }

    // std::cout << "approx_data(), determined block intervals, time elapsed: " << timer.elapsed() << "s.\n";
    // timer.reset();

    std::vector<IndexPair> indices; indices.resize(input_points.size());
    std::vector<int> l_vec; l_vec.resize(m_ndims);
    int count {0};
    for (const auto &point : input_points) {
        for (int d=0; d<m_ndims; ++d) {
            l_vec[d] = search_interval(point[d], block_intervals[d]).min;
        }
        // get corresponding block index
        indices[count].iBlock = get_index(l_vec, m_shape_blocks);
        indices[count].iPoint = count;
        ++count;
    }

    // std::cout << "approx_data(), determined block indices, time elapsed: " << timer.elapsed() << "s.\n";
    // timer.reset();

    // 2. Sort in order of block indices
    sort_indices(indices, IndexAtt::BLOCK);

    // std::cout << "approx_data(), points sorted by block, time elapsed: " << timer.elapsed() << "s.\n";
    // timer.reset();

    // 3. Decode blocks and approximate data
    count = 0;
    int I;
    double *dct_decoded_I = new double[m_block_size];
    double *idct_I        = new double[m_block_size];
    Interval *intervals   = new Interval[m_ndims];
    std::vector<std::vector<double>> local_points; // points within block I
    local_points.resize(m_ndims);
    for (int d=0; d<m_ndims; ++d) local_points[d].resize(m_block_shape[d]);
    std::vector<std::vector<int>> bound_points;
    std::vector<double> bound_data;
    output_data.resize(input_points.size());
    for (const auto &index_pair : indices) {

        // timer.reset();

        if (!count || index_pair.iBlock != I) {
            // 3.1 Decode block I
            I = index_pair.iBlock;
            set_index_vector(I, l_vec, m_shape_blocks);
            decode_dct_block(I /* block index */, dct_decoded_I);

            // std::cout << "decoded block, time elapsed: " << timer.elapsed() << "s.\n";
            // timer.reset();

            compute_idct_block(dct_decoded_I, idct_I);

            // std::cout << "computed idct, time elapsed: " << timer.elapsed() << "s.\n";
            // timer.reset();
        }

        // 3.2 Linear interpolation

        // 3.2.1 Determine the upper and lower bounding values for each dimension, and use this to determine the
        // bounding data points within this block.
        for (int d=0; d<m_ndims; ++d) {
            for (int v=0; v<m_block_shape[d]; ++v) {
                local_points[d][v] = m_input_points[d][l_vec[d]*(m_block_shape[d]-1) + v];
            }
            intervals[d] = search_interval(input_points[index_pair.iPoint][d], local_points[d]);
        }

        // 3.2.2 Determine bounding points
        bound_points.resize(1);
        bound_points[0].clear();
        for (int d=0; d<m_ndims; ++d) {
            const int NUM_POINTS {static_cast<int>(bound_points.size())};
            for (int k=0; k<NUM_POINTS; ++k) {
                if (intervals[d].max != intervals[d].min) { // point doesn't lie EXACTLY at an interval so we need to interpolate
                    std::vector<int> point {bound_points[k]};
                    bound_points.push_back(point);
                    bound_points[NUM_POINTS+k].push_back(intervals[d].max);
                }
                bound_points[k].push_back(intervals[d].min);
            }
        }

        // 3.2.3 Determine data values at each bounding point
        bound_data.resize( bound_points.size() );
        for (int p=0; p<static_cast<int>(bound_points.size()); ++p) {
            // determine block index for point
            const int j {get_index(bound_points[p], m_block_shape)};
            bound_data[p] = idct_I[j];
        }

        // 3.2.4 Perform linear interpolation
        for (int d=m_ndims-1; d>=0; --d) {
            // determine the fraction of the interval in this dimension
            const double LAMBDA { (input_points[index_pair.iPoint][d] - local_points[d][intervals[d].min]) /
                                  (local_points[d][intervals[d].max] - local_points[d][intervals[d].min]) };
            const int NUM_BOUNDS {static_cast<int>(bound_data.size())/2};
            std::vector<double> new_bound_data; new_bound_data.resize(NUM_BOUNDS);
            for (int p=0; p<NUM_BOUNDS; ++p) {
                new_bound_data[p] = (1-LAMBDA)*bound_data[p] + LAMBDA*bound_data[NUM_BOUNDS+p];
            }
            bound_data = new_bound_data;
        }

        // 4. Spline interpolation (e.g. using SPLINTER -- currently requires linear spacing) could be added later if
        // desired.

        output_data[index_pair.iPoint] = bound_data[0];

        // std::cout << "linear interpolation, time elapsed: " << timer.elapsed() << "s.\n";
        // timer.reset();
    }

    // std::cout << "approx_data(), blocks decoded and data interpolated, time elapsed: " << timer.elapsed() << "s.\n";
    // timer.reset();

    delete[] dct_decoded_I;
    delete[] idct_I;
    delete[] intervals;
}

void DataProcessor::approx_data(const std::vector<std::vector<double>> &input_points, std::vector<double> &output_data) {
    assert(m_approximated && "Need to compute inverse cosine transform.\n");
    assert(m_points_set && "Need to set input space points.\n");
    for (const auto &point : input_points)
        assert(static_cast<int>(point.size()) == m_ndims && "input_point dimensions don't match those of the input space.\n");

    // Linear interpolation
    Interval *intervals = new Interval[m_ndims];
    std::vector<std::vector<int>> bound_points;
    std::vector<double> bound_data;
    output_data.resize(input_points.size());
    int iPoint {0};

    // std::cout << "approx_data() debug 2\n";

    for (const auto &point_in : input_points) {
        // determine intervals
        for (int d=0; d<m_ndims; ++d) {
            intervals[d] = search_interval(point_in[d], m_input_points[d]);
        }

        // std::cout << "approx_data() debug 3\n";

        // determine bounding points
        bound_points.resize(1);
        bound_points[0].clear();
        for (int d=0; d<m_ndims; ++d) {
            const int NUM_POINTS {static_cast<int>(bound_points.size())};
            for (int k=0; k<NUM_POINTS; ++k) {
                if (intervals[d].max != intervals[d].min) { // point doesn't lie EXACTLY at an interval so we need to interpolate
                    std::vector<int> point {bound_points[k]};
                    bound_points.push_back(point);
                    bound_points[NUM_POINTS+k].push_back(intervals[d].max);
                }
                bound_points[k].push_back(intervals[d].min);
            }
        }

        // std::cout << "approx_data() debug 4\n";

        // determine data values at bounding points
        bound_data.resize( bound_points.size() );
        for (int p=0; p<static_cast<int>(bound_points.size()); ++p) {
            // determine index for point
            const int i {get_index(bound_points[p], m_shape)};

            // std::cout << "approx_data() debug 5\n";

            bound_data[p] = m_approx[i];
        }

        // std::cout << "approx_data() debug 6\n";

        // perform linear interpolation
        for (int d=m_ndims-1; d>=0; --d) {
            // determine the fraction of the interval in this dimension
            const double LAMBDA { (point_in[d] - m_input_points[d][intervals[d].min]) /
                                  (m_input_points[d][intervals[d].max] - m_input_points[d][intervals[d].min]) };
            const int NUM_BOUNDS {static_cast<int>(bound_data.size())/2};
            std::vector<double> new_bound_data; new_bound_data.resize(NUM_BOUNDS);
            for (int p=0; p<NUM_BOUNDS; ++p) {
                new_bound_data[p] = (1-LAMBDA)*bound_data[p] + LAMBDA*bound_data[NUM_BOUNDS+p];
            }
            bound_data = new_bound_data;
        }

        // std::cout << "approx_data() debug 7\n";

        output_data[iPoint] = bound_data[0];
        ++iPoint;
    }
    delete[] intervals;
}

}
