#ifndef ENCODE_H_
#define ENCODE_H_

#include <fftw3.h>

#include <vector>
#include <list>
// #include "H5Cpp.h"
#include <string>

namespace api {
struct compressed_data {
    std::vector<int> shape;
    std::vector<int> block_shape;
    std::vector<double> steps;
    unsigned char extra_code_bytes[1];
    std::vector<std::vector<unsigned char>> symbols;
    std::vector<std::vector<unsigned char>> codes;
    std::vector<std::vector<unsigned char>> data;
};
}

namespace data_comp {

// labelling bits in order from left to right
const unsigned char BIT0 {0b10000000};
const unsigned char BIT1 {0b01000000};
const unsigned char BIT2 {0b00100000};
const unsigned char BIT3 {0b00010000};
const unsigned char BIT4 {0b00001000};
const unsigned char BIT5 {0b00000100};
const unsigned char BIT6 {0b00000010};
const unsigned char BIT7 {0b00000001};
const std::vector<unsigned char> BITS {BIT0, BIT1, BIT2, BIT3, BIT4, BIT5, BIT6, BIT7};

// void fft_test();

enum class LeafAttribute {
    SYMBOL,
    FREQ
};

enum class Child {
    ZERO,
    ONE
};

enum class Spacing {
    LIN,
    POLY,
    EXP
};

struct Range {
    double min;
    double max;
};

struct Interval {
    int min;
    int max;
};

struct IndexPair {
    int iPoint;
    int iBlock;
};

enum class IndexAtt {
    POINT,
    BLOCK
};

class Node {
protected:
    double m_freq;
    Node *m_parent {nullptr};
public:
    Node();
    Node(double freq);
    virtual ~Node();
    virtual char get_type() const;
    double get_freq() const;
    Node* get_parent() const;
    virtual int get_symbol() const;
    virtual void get_code(std::vector<unsigned char> &code_out) const;
    virtual Node* get_child(Child bit) const;
    void set_freq(double freq);
    void set_parent(Node *parent);
    virtual void set_symbol(int symbol);
    virtual void set_code(const std::vector<unsigned char> &code_in);
    virtual void set_child(Child bit, Node *child);
};

class Leaf : public Node {
protected:
    int m_symbol;
    std::vector<unsigned char> m_code;
public:
    Leaf();
    Leaf(int symbol, double freq);
    virtual char get_type() const;
    virtual int get_symbol() const;
    virtual void get_code(std::vector<unsigned char> &code_out) const;
    virtual Node* get_child(Child bit) const;
    virtual void set_symbol(int symbol);
    virtual void set_code(const std::vector<unsigned char> &code_in);
    virtual void set_child(Child bit, Node *child);
};

class Branch : public Node {
protected:
    Node *m_child0 {nullptr}, *m_child1 {nullptr};
public:
    Branch();
    Branch(Node *child0, Node *child1, double freq);
    virtual char get_type() const;
    virtual int get_symbol() const;
    virtual void get_code(std::vector<unsigned char> &code_out) const;
    virtual Node* get_child(Child bit) const;
    virtual void set_symbol(int symbol);
    virtual void set_code(const std::vector<unsigned char> &code_in);
    virtual void set_child(Child bit, Node *child);
};

struct Tree {
    std::list<Leaf> leaves;
    std::list<Branch> branches;
    Branch root;
};

class DataProcessor {
private:
    bool m_allocated=false;
    std::vector<double> m_dataset;
    std::vector<int> m_shape;// shape of overall dataset
    std::vector<int> m_block_shape; // shape of a single block
    int m_ndims, m_size, m_block_size; // number of dimensions of data, total size of data, total size of a single block
    bool m_got_data=false;
    bool m_got_dct=false;
    bool m_compressed=false;
    std::vector<int> m_shape_blocks; // shape of entire dataset in terms of blocks
    int m_num_blocks; // total number of blocks
    std::vector<double *> m_block_data;
    std::vector<double *> m_dcts;
    std::vector<double *> m_dcts_lossy;
    std::vector<int *> m_dcts_quant;
    bool m_quantized=false;
    std::vector<double> m_step_size;
    std::vector<double> m_approx; // approximated data computed from decoding and applying idct
    Tree m_tree;
    std::vector<Leaf> m_codes;
    std::vector<std::vector<unsigned char>> m_dcts_encoded;
    std::vector<double *> m_dcts_decoded;
    bool m_encoded=false;
    bool m_decoded=false;
    bool m_approximated=false;
    int m_num_bits;
    bool m_points_set=false;
    bool m_spacings_set=false;
    bool m_ranges_set=false;
    std::vector<Spacing> m_spacings;
    std::vector<Range> m_ranges;
    std::vector<std::vector<double>> m_input_points;

    void allocate_variables();
    void deallocate_variables();
    void assign_variables(const double *step_size_data, const int EXTRA_BYTES,
                          const unsigned char *const *const symbols_data, const unsigned char *const *const codes_data,
                          const std::vector<unsigned char *> &data, const int NUM_SYMBOLS,
                          const std::vector<int> &data_bytes, const int SYMBOL_BYTES);
    void decode_dct_block(const int I, double *dct_decoded_out);
    void compute_idct_block(const double *dct_in, double *idct_out);
public:
    DataProcessor() {}
    DataProcessor(const std::vector<int> &shape, const std::vector<int> &block_shape);
    ~DataProcessor();
    void add_dataset(const std::vector<double> &dataset);
    void compute_dct();
    void compress_dct(const std::vector<int> &shape_to_keep);
    void quantize_dct(int num_bits);
    void encode_dct();
    void decode_dct();
    void compute_idct(bool compressed);
    // void write_data(const std::string FILENAME);
    // void read_data(const std::string FILENAME);
    void set_data(const api::compressed_data &data_struct);
    void set_spacings(const std::vector<int> &spacings);
    void set_ranges(const std::vector<double> &mins, const std::vector<double> &maxes);
    void set_input_points();
    double approx_data_encoded(const std::vector<double> &input_point);
    void approx_data_encoded(const std::vector<std::vector<double>> &input_points, std::vector<double> &output_data);
    void approx_data(const std::vector<std::vector<double>> &input_points, std::vector<double> &output_data);
};

}

#endif /* ENCODE_H_ */
