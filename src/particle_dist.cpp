#include "particle_dist.h"
#include <iostream>
#include <stdexcept>
#include <cassert>
#include "boost/random/mersenne_twister.hpp"
// #include "boost/random/random_device.hpp"
#include <random>
#include "boost/random/normal_distribution.hpp"

namespace pdist {

// #include <chrono>

// class Timer {
// private:
//     using clock_t = std::chrono::high_resolution_clock;
//     using second_t = std::chrono::duration<double, std::ratio<1> >;
    
//     std::chrono::time_point<clock_t> m_beg;
// public:
//     Timer() : m_beg(clock_t::now()) {}
    
//     void reset() {
//         m_beg = clock_t::now();
//     }
    
//     double elapsed() const {
//         return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
//     }
// };

double rand_unif() {
    // returns random number in [0,1)
    static std::random_device rd;
    static std::mt19937 mersenne{ rd() };
    static const double fraction = 1.0 / (static_cast<double>(mersenne.max()) + 1.0);
    return mersenne() * fraction;
}

bool multi_norm(const int N_SAMPLES, const Eigen::VectorXd &Mu, const Eigen::MatrixXd &Sigma, Eigen::MatrixXd &samples_out) {
    const int N_DIMS {static_cast<int>(Mu.size())};

    // Generate independent standard normal samples
    static std::random_device rd; // generator
    static std::mt19937 mersenne{ rd() }; // uniform random numbers
    boost::normal_distribution<double> Ndist;

    Eigen::MatrixXd StdNorm;
    StdNorm.resize(N_DIMS, N_SAMPLES);
    for (int d=0; d<N_DIMS; ++d) {
        for (int s=0; s<N_SAMPLES; ++s) {
            StdNorm(d,s) = Ndist(mersenne);
        }
    }

    // transform using covariance matrix Sigma

    // fastest/least accurate
    // compute matrix A s.t. A*A.transpose() = Sigma
    Eigen::LLT<Eigen::MatrixXd> lltOfSigma(Sigma);
    bool positive {lltOfSigma.info() == Eigen::Success};
    Eigen::MatrixXd LLTmatrixA {lltOfSigma.matrixL()};
    // Eigen::MatrixXd LLTmatrixA {Sigma.llt().matrixL()};

    // not as fast as LLT, but better accuracy
    // Eigen::LDLT<Eigen::MatrixXd> ldltOfSigma(Sigma);
    // Sigma.ldlt().isPositive();
    // Eigen::MatrixXd LDLTmatrixL {ldltOfSigma.matrixL()};
    // Eigen::PermutationMatrix<Eigen::Dynamic> LDLTmatrixP {ldltOfSigma.transpositionsP()};

    // compute matrix A s.t. A*A.transpose() = Sigma
    // Eigen::MatrixXd LDLTmatrixA {LDLTmatrixP.transpose() * LDLTmatrixL * ldltOfSigma.vectorD().cwiseSqrt().asDiagonal()};

    // slowest/best accuracy
    // Eigen::BDCSVD<Eigen::MatrixXd> svdOfSigma(Sigma, Eigen::ComputeFullU);
    // Eigen::MatrixXd U {svdOfSigma.matrixU()};
    // Eigen::VectorXd S {svdOfSigma.singularValues()};

    // compute matrix A s.t. A*A.transpose() = Sigma
    // Eigen::MatrixXd SVDmatrixA {U*S.cwiseSqrt().asDiagonal()};
    
    // compare transformations:
    // Eigen::MatrixXd LLTsamples {(LLTmatrixA*StdNorm).colwise() + Mu};
    // Eigen::MatrixXd LDLTsamples {(LDLTmatrixA*StdNorm).colwise() + Mu};
    // Eigen::MatrixXd SVDsamples {(SVDmatrixA*StdNorm).colwise() + Mu};

    // samples_out = (LDLTmatrixA*StdNorm).colwise() + Mu;
    samples_out = (LLTmatrixA*StdNorm).colwise() + Mu;

    // return ldltOfSigma.isPositive();
    return positive;
}

void ParticleDist::set_ndims(const int NDIMS) {
    m_ndims = NDIMS;
    m_ndims_set = true;
}

void ParticleDist::set_Mu(const std::vector<double> &Mu_in) {
    assert(m_ndims_set && "Number of dimensions not set.\n");
    assert(static_cast<int>(Mu_in.size()) == m_ndims && "Dimensions of Mu_in don't match those of m_ndims.\n");
    m_Mu.resize(m_ndims);
    for (int n_i=0; n_i<m_ndims; ++n_i) {
        m_Mu(n_i) = Mu_in[n_i];
    }
    m_Mu_set = true;
}

void ParticleDist::set_Sigma(const std::vector<std::vector<double>> &Sigma_in) {
    assert(m_ndims_set && "Number of dimensions not set.\n");
    assert(static_cast<int>(Sigma_in.size()) == m_ndims && "Dimensions of Sigma_in rows don't match those of m_ndims.\n");
    m_Sigma.resize(m_ndims, m_ndims);
    for (int n_i=0; n_i<m_ndims; ++n_i) {
        assert(static_cast<int>(Sigma_in[n_i].size()) == m_ndims && "Dimensions of Sigma_in don't match those of m_ndims.\n");
        for (int n_j=0; n_j<m_ndims; ++n_j) {
            m_Sigma(n_i,n_j) = Sigma_in[n_i][n_j];
        }
    }
    m_Sigma_set = true;
}

ParticleDist::ParticleDist(const std::vector<double> &Mu, const std::vector<std::vector<double>> &Sigma) : m_MIN_PTLS{30} {
    set_ndims(static_cast<int>(Mu.size()));
    set_Mu(Mu);
    set_Sigma(Sigma);
}

ParticleDist::ParticleDist(const std::vector<double> &Mu, const std::vector<std::vector<double>> &Sigma, const int MIN_PTLS) :
    m_MIN_PTLS{MIN_PTLS}
{
    set_ndims(static_cast<int>(Mu.size()));
    set_Mu(Mu);
    set_Sigma(Sigma);
}

int ParticleDist::get_ndims() const {
    assert(m_ndims_set && "Number of dimensions not set.\n");
    return m_ndims;
}

void ParticleDist::get_Mu(std::vector<double> &Mu_out) const {
    assert(m_Mu_set && "Mu has not been set.");
    Mu_out.resize(m_ndims);
    for (int n_i=0; n_i<m_ndims; ++n_i) {
        Mu_out[n_i] = m_Mu(n_i);
    }
}

void ParticleDist::get_Sigma(std::vector<std::vector<double>> &Sigma_out) const {
    assert(m_Sigma_set && "Sigma has not been set.\n");
    Sigma_out.resize(m_ndims);
    for (int n_i=0; n_i<m_ndims; ++n_i) {
        Sigma_out[n_i].resize(m_ndims);
        for (int n_j=0; n_j<m_ndims; ++n_j) {
            Sigma_out[n_i][n_j] = m_Sigma(n_i,n_j);
        }
    }
}

void reset_Sigma(Eigen::MatrixXd &Sigma) {
    const int NDIMS {static_cast<int>(Sigma.cols())}; // Sigma should always be square, so Sigma.rows() should give the
                                                      // same thing
    Eigen::VectorXd diag; diag.resize(NDIMS);
    for (int i=0; i<NDIMS; ++i) {
        diag(i) = std::abs(Sigma(i,i));
    }
    Sigma.setZero();
    for (int i=0; i<NDIMS; ++i) {
        Sigma(i,i) = diag(i);
    }
}

bool ParticleDist::generate_particles(const int N_PTLS, const std::vector<double> &inputs,
                                      Eigen::MatrixXd &InputPoints_out, Eigen::MatrixXd &Particles_out) {
    Particles_out.resize(m_ndims, N_PTLS);
    InputPoints_out.resize(inputs.size(), N_PTLS);
    Eigen::MatrixXd particle;
    Eigen::VectorXd input_point;
    int count {0};
    int loops {0};
    while (count < N_PTLS) {
        bool pos_def {multi_norm(1, m_Mu, m_Sigma, particle)};
        if (!pos_def) {
            reset_Sigma(m_Sigma);
            pos_def = multi_norm(1, m_Mu, m_Sigma, particle);
            if (!pos_def) throw std::runtime_error("Covariance matrix not positive definite even after reset!");
        }
        get_input_point(particle.col(0), inputs, input_point);
        if (check_ranges(input_point)) {
            InputPoints_out.col(count) = input_point;
            Particles_out.col(count) = particle.col(0);
            ++count;
        }
        ++loops;
        if (loops > 30*N_PTLS) return false; // prevent infinite loop
    }
    return true;
}

bool ParticleDist::update(const std::vector<int> &results, const std::vector<double> &inputs,
                          const int N_PTLS, const double KAPPA, const double UNCERTAINTY
                          // for testing:
                          // std::vector<std::vector<double>> &prior_particles_out,
                          // std::vector<double> &particle_likelihoods_out,
                          // std::vector<std::vector<double>> &posterior_particles_out
    ) {
    
    // Profiling
    // Timer timer;

    const int NUM_RESULTS {static_cast<int>(results.size())};

    Eigen::MatrixXd Particles;
    Eigen::MatrixXd InputPoints;
    
    bool ptls_generated {generate_particles(N_PTLS, inputs, InputPoints, Particles)};

    if (!ptls_generated) return false;

    // std::cout << "Generated particles, elapsed time: " << timer.elapsed() << "s.\n";
    // timer.reset();

    // debugging
    bool is_nan {false};
    for (int p=0; p<N_PTLS; ++p) {
        for (int i=0; i<m_ndims; ++i) {
            if (std::isnan(Particles(i,p))) {
                std::cout << "m_Mu:\n" << m_Mu << "\n";
                std::cout << "m_Sigma:\n" << m_Sigma << "\n";
                std::cout << "Particles:\n" << Particles << "\n";
                is_nan = true;
                break;
            }
        }
        if (is_nan) break;
    }
    
    Eigen::VectorXd single_shot_likelihoods;
    Eigen::VectorXd likelihoods; likelihoods.resize(N_PTLS); likelihoods.setOnes();
    bool all_results {true};
    for (const auto &result : results) {
        if (!result) {
            all_results = false;
            break;
        }
    }
    Eigen::VectorXd total_likelihoods;
    if (all_results) {
        total_likelihoods.resize(N_PTLS);
        total_likelihoods.setZero();
    }
    int count {0};
    for (int r=0; r<NUM_RESULTS; ++r) {
        if (results[r]) {
            if (NUM_RESULTS - 1 - count) {
                get_likelihoods(r, InputPoints, single_shot_likelihoods);
                if (all_results) {
                    total_likelihoods += single_shot_likelihoods;
                    ++count;
                }
            } else {
                single_shot_likelihoods = 1.0 - total_likelihoods.array();
            }
            likelihoods.array() *= single_shot_likelihoods.array().pow(results[r]);
        }
    }

    // std::cout << "Obtained likelihoods, elapsed time: " << timer.elapsed() << "s.\n";
    // timer.reset();
    
    Eigen::VectorXd Mu_acc; Mu_acc.resize(m_ndims);
    Eigen::VectorXd Var_acc; Var_acc.resize( (m_ndims*(m_ndims+1))/2 ); // number of independent variables in Sigma
    Mu_acc.setZero(); Var_acc.setZero();
    int N_acc {0};

    // When we have results from multiple measurements, automatically determine KAPPA depending on the likelihood data
    int num_shots {0};
    double kappa;
    for (const auto &result : results) {
        num_shots += result;
    }
    if (num_shots > 1)
        kappa = likelihoods.maxCoeff();
    else
        kappa = KAPPA;

    // for testing:
    // std::vector<Eigen::VectorXd> particles_acc;
    
    for (int p=0; p<N_PTLS; ++p) {
        if (likelihoods(p) > kappa*rand_unif()) { // accumulate
            Mu_acc += Particles.col(p);
            count = 0;
            for (int i=0; i<m_ndims; ++i) {
                for (int j=0; j<(m_ndims-i); ++j) {
                    Var_acc(count+j) += Particles(i,p)*Particles(j+i,p);
                }
                count += m_ndims-i;
            }
            ++N_acc;

            // for testing:
            // particles_acc.push_back(Particles.col(p));
        }
    }

    // std::cout << "Filterd particles (N_acc = " << N_acc << "), elapsed time: " << timer.elapsed() << "s.\n";
    // timer.reset();
    
    if (N_acc < m_MIN_PTLS) {
        std::cout << "Warning: less than " << m_MIN_PTLS << " particles were accumulated; not updating distribution.\n";
    } else {
        m_Mu = Mu_acc/N_acc;

        count = 0;
        for (int i=0; i<m_ndims; ++i) {
            for (int j=0; j<(m_ndims-i); ++j) {
                m_Sigma(i, j+i) = Var_acc(count+j)/N_acc - m_Mu(i)*m_Mu(j+i);
            }
            count += m_ndims-i;
        }
        for (int i=1; i<m_ndims; ++i) {
            for (int j=0; j<i; ++j) {
                m_Sigma(i, j) = m_Sigma(j, i);
            }
        }

        m_Sigma *= UNCERTAINTY;
        
        std::cout << "Updated distribution:\n";
        std::cout << "Mu:\n" << m_Mu << "\n";
        std::cout << "Sigma:\n" << m_Sigma << "\n";

        // debugging
        for (int i=0; i<m_ndims; ++i) {
            if (std::isnan(m_Mu(i))) {
                std::cout << "Mu_acc:\n" << Mu_acc << "\n";
                std::cout << "Var_acc:\n" << Var_acc << "\n";
                std::cout << "Particles:\n" << Particles << "\n";
                break;
            }
        }
    }

    // std::cout << "ParticleDist::update(), remaining elapsed time: " << timer.elapsed() << "s.\n";

    // for testing:
    // prior_particles_out.resize(N_PTLS);
    // particle_likelihoods_out.resize(N_PTLS);
    // for (int p=0; p<N_PTLS; ++p) {
    //     particle_likelihoods_out[p] = likelihoods(p);
    //     prior_particles_out[p].resize(m_ndims);
    //     for (int d=0; d<m_ndims; ++d) {
    //         prior_particles_out[p][d] = Particles(d,p);
    //     }
    // }
    // posterior_particles_out.resize(N_acc);
    // for (int pa=0; pa<N_acc; ++pa) {
    //     posterior_particles_out[pa].resize(m_ndims);
    //     for (int d=0; d<m_ndims; ++d) {
    //         posterior_particles_out[pa][d] = particles_acc[pa](d);
    //     }
    // }

    return true;
}

//void ParticleDist::get_opt_controls(std::vector<double> &controls_out, const std::vector<double> &mins,
//                                    const std::vector<double> &maxes, const int MAX_CALLS) {
//    typedef dlib::matrix<double,0,1> column_vector;
//    const int NUM_CONTROLS {static_cast<int>(mins.size())};
//
//    column_vector mins_dlib, maxes_dlib;
//    mins_dlib.set_size(NUM_CONTROLS); maxes_dlib.set_size(NUM_CONTROLS);
//    for (int c=0; c<NUM_CONTROLS; ++c) {
//        mins_dlib(c) = mins[c];
//        maxes_dlib(c) = maxes[c];
//    }
//
//    auto result = dlib::find_max_global(NegativeVariance(this), mins_dlib, maxes_dlib, dlib::max_function_calls(MAX_CALLS));
//    std::cout << "Optimal gain found: " << result.y << "\n";
//    controls_out.resize(NUM_CONTROLS);
//    for (int c=0; c<NUM_CONTROLS; ++c) {
//        controls_out[c] = result.x(c);
//    }
//}

//EntropyGain::EntropyGain(ParticleDist *const dist) : m_dist {dist} {}
//
//double EntropyGain::operator()(const dlib::matrix<double,0,1> &controls) {
//    const int SIZE {static_cast<int>(controls.size())};
//    std::vector<double> controls_vec; controls_vec.resize(SIZE);
//    for (int i=0; i<SIZE; ++i) controls_vec[i] = controls(i);
//    return m_dist->get_entropy_gain(controls_vec);
//}
//
//NegativeVariance::NegativeVariance(ParticleDist *const dist) : m_dist {dist} {}
//
//double NegativeVariance::operator()(const dlib::matrix<double,0,1> &controls) {
//    const int SIZE {static_cast<int>(controls.size())};
//    std::vector<double> controls_vec; controls_vec.resize(SIZE);
//    for (int i=0; i<SIZE; ++i) controls_vec[i] = controls(i);
//    return m_dist->get_NV(controls_vec);
//}

DistExpMS::DistExpMS() : m_3D{false} {set_ndims(Param2D::NUM_PARAMS);}

DistExpMS::DistExpMS(const int MIN_PTLS, const bool _3D) : ParticleDist(MIN_PTLS), m_3D{_3D} {
    if (m_3D)
        set_ndims(Param::NUM_PARAMS);
    else
        set_ndims(Param2D::NUM_PARAMS);
}

DistExpMS::DistExpMS(const std::vector<double> &Mu, const std::vector<std::vector<double>> &Sigma) : m_3D{false} {
    set_ndims(Param2D::NUM_PARAMS);
    set_Mu(Mu);
    set_Sigma(Sigma);
}

DistExpMS::DistExpMS(const std::vector<double> &Mu, const std::vector<std::vector<double>> &Sigma, const int MIN_PTLS, const bool _3D) :
    ParticleDist(MIN_PTLS), m_3D{_3D}
{
    if (m_3D)
        set_ndims(Param::NUM_PARAMS);
    else
        set_ndims(Param2D::NUM_PARAMS);
    set_Mu(Mu);
    set_Sigma(Sigma);
}

// void DistExpMS::set_kappas(const std::vector<double> &kappas) {
//     assert(static_cast<int>(kappas.size()) == Result::NUM_RESULTS &&
//            "kappas size doesn't match number of possible measurement results.\n");
//     m_kappas.resize(Result::NUM_RESULTS);
//     for (int r=0; r<Result::NUM_RESULTS; ++r) {
//         m_kappas(r) = kappas[r];
//     }
//     m_kappas_set = true;
// }

void DistExpMS::set_ranges(const std::vector<std::vector<double>> &ranges, const bool SMALL) {
    assert(static_cast<int>(ranges.size()) == m_NDIMS_CONTROLS && "Number of input ranges doesn't match dimensions of dataset.\n");
    std::vector<data_comp::Range> &M_RANGES {(SMALL) ? m_ranges_small : m_ranges};
    M_RANGES.resize(m_NDIMS_CONTROLS);
    for (int d=0; d<m_NDIMS_CONTROLS; ++d) {
        assert(ranges[d].size() == 2 && "Each range should contain two values.\n");
        M_RANGES[d].min = ranges[d][0];
        M_RANGES[d].max = ranges[d][1];
    }
    if (SMALL)
        m_ranges_set_small = true;
    else
        m_ranges_set = true;
}

void DistExpMS::set_data(const int RESULT, const api::compressed_data &data_struct, const std::vector<int> &spacings,
                         const std::vector<double> &mins, const std::vector<double> &maxes, const bool SMALL) {
    switch (RESULT) {
    case Result::GG:
        if (SMALL) {
            m_gg_data_small.set_data(data_struct);
            m_gg_data_small.decode_dct();
            m_gg_data_small.compute_idct(true);
            m_gg_data_small.set_spacings(spacings);
            m_gg_data_small.set_ranges(mins, maxes);
            m_gg_data_small.set_input_points();
            m_gg_file_set_small = true;
        } else {
            m_gg_data.set_data(data_struct);
            m_gg_data.decode_dct();
            m_gg_data.compute_idct(true);
            m_gg_data.set_spacings(spacings);
            m_gg_data.set_ranges(mins, maxes);
            m_gg_data.set_input_points();
            m_gg_file_set = true;
        }
        break;
    case Result::EE:
        if (SMALL) {
            m_ee_data_small.set_data(data_struct);
            m_ee_data_small.decode_dct();
            m_ee_data_small.compute_idct(true);
            m_ee_data_small.set_spacings(spacings);
            m_ee_data_small.set_ranges(mins, maxes);
            m_ee_data_small.set_input_points();
            m_ee_file_set_small = true;
        } else {
            m_ee_data.set_data(data_struct);
            m_ee_data.decode_dct();
            m_ee_data.compute_idct(true);
            m_ee_data.set_spacings(spacings);
            m_ee_data.set_ranges(mins, maxes);
            m_ee_data.set_input_points();
            m_ee_file_set = true;
        }
        break;
    default:
        std::cout << "Warning: set_data() method called with invalid RESULT, no files were set.\n";
        break;
    }
}

// double DistExpMS::get_kappa(const int RESULT) const {
//     assert(m_kappas_set && "Need to set kappas.\n");
//     assert(RESULT < Result::NUM_RESULTS && "Invalid Result.\n");
//     return m_kappas(RESULT);
// }

void DistExpMS::get_input_point(const Eigen::VectorXd &particle,
                                const std::vector<double> &inputs, Eigen::VectorXd &input_point_out) const {
    const double D_C {particle(Param::DELTA)/2}; // characteristic detuning
    const double T_C {4.0*M_PI/particle(Param::DELTA)}; // characteristic time
    input_point_out.resize(m_NDIMS_CONTROLS);
    input_point_out(Param::DELTA) = inputs[Param::DELTA]/D_C;
    input_point_out(Param::DELTA0) = (inputs[Param::DELTA0] - particle(Param::DELTA0))/D_C;
    if (m_3D) {
        const double T_STAR {0.5}; // dimensionless optimal gate time
        input_point_out(Param::TAU) = (inputs[Param::TAU] - particle(Param::TAU))/T_C + T_STAR;
    } else {
        input_point_out(Param::TAU) = inputs[Param::TAU]/T_C;
    }
}

bool DistExpMS::check_ranges(const Eigen::VectorXd &input_point) const {
    assert(m_ranges_set && "Need to set ranges.\n");
    for (int d=0; d<m_NDIMS_CONTROLS; ++d) {
        if ((input_point(d) < m_ranges[d].min) || (input_point(d) > m_ranges[d].max)) return false;
    }
    return true;
}

void DistExpMS::get_likelihoods(const int RESULT, const Eigen::MatrixXd &InputPoints, Eigen::VectorXd &likelihoods_out) {
    // Timer timer;
    
    assert(m_gg_file_set && "Need to set file for GG data.\n");
    assert(m_ee_file_set && "Need to set file for EE data.\n");

    const long int NUM_POINTS {InputPoints.cols()};
    std::vector<std::vector<double>> input_points, input_points_small;
    std::vector<int> indices, indices_small;
    input_points.reserve(NUM_POINTS);
    const bool SMALL {m_gg_file_set_small && m_ee_file_set_small && m_ranges_set_small};
    if (SMALL) {
        input_points_small.reserve(NUM_POINTS);
        indices.reserve(NUM_POINTS);
        indices_small.reserve(NUM_POINTS);
    }
    for (int i=0; i<NUM_POINTS; ++i) {
        bool small_point {false};
        if (SMALL) {
            small_point = true;
            for (int d=0; d<m_NDIMS_CONTROLS; ++d) {
                if ((InputPoints(d,i) <= m_ranges_small[d].min) || (InputPoints(d,i) >= m_ranges_small[d].max)) {
                    small_point = false;
                    break;
                }
            }
            if (small_point)
                indices_small.push_back(i);
            else
                indices.push_back(i);
        }
        std::vector<double> point; point.resize(m_NDIMS_CONTROLS);
        for (int d=0; d<m_NDIMS_CONTROLS; ++d)
            point[d] = InputPoints(d,i);
        if (small_point)
            input_points_small.push_back(point);
        else
            input_points.push_back(point);
    }

    // std::cout << "Converted input_points to vector, time elapsed: " << timer.elapsed() << "s.\n";
    // timer.reset();

    std::vector<double> output_data, output_data_small;
    switch (RESULT) {
    case Result::GG:
        if (input_points_small.size() > 0) m_gg_data_small.approx_data(input_points_small, output_data_small);
        if (input_points.size() > 0) m_gg_data.approx_data(input_points, output_data);
        break;
    case Result::EE:
        if (input_points_small.size() > 0) m_ee_data_small.approx_data(input_points_small, output_data_small);
        if (input_points.size() > 0) m_ee_data.approx_data(input_points, output_data);
        break;
    case Result::GEEG:
        {
            std::vector<double> output_data_ee, output_data_ee_small;
            if (input_points_small.size() > 0) {
                m_gg_data_small.approx_data(input_points_small, output_data_small);
                m_ee_data_small.approx_data(input_points_small, output_data_ee_small);
            }
            if (input_points.size() > 0) {
                m_gg_data.approx_data(input_points, output_data);
                m_ee_data.approx_data(input_points, output_data_ee);
            }
            for (int j=0; j<static_cast<int>(input_points.size()); ++j) {
                double geeg_likelihood {1.0 - output_data[j] - output_data_ee[j]};
                output_data[j] = (geeg_likelihood > 0.0) ? geeg_likelihood : 0.0;
            }
            for (int js=0; js<static_cast<int>(input_points_small.size()); ++js) {
                double geeg_likelihood {1.0 - output_data_small[js] - output_data_ee_small[js]};
                output_data_small[js] = (geeg_likelihood > 0.0) ? geeg_likelihood : 0.0;
            }
            break;
        }
    default:
        throw std::runtime_error("Invalid RESULT.\n");
    }

    // std::cout << "Approximated likelihood data, time elapsed: " << timer.elapsed() << "s.\n";
    // timer.reset();
    
    likelihoods_out.resize(NUM_POINTS);
    for (int j=0; j<static_cast<int>(input_points.size()); ++j) {
        const int i {(SMALL) ? indices[j] : j};
        likelihoods_out(i) = output_data[j];
    }
    for (int js=0; js<static_cast<int>(input_points_small.size()); ++js) {
        likelihoods_out(indices_small[js]) = output_data_small[js];
    }

    // std::cout << "Converted likelihoods to Eigen::VectorXd, time elapsed: " << timer.elapsed() << "s.\n";
    
    // Note: should consider either using std::vectors or Eigen::vectors here and in the approx_data() method of
    // DataProcessor to avoid having to copy values back and forth.
}

// void DistExpMS::set_gain_particles(const int N_PTLS) {
//     m_gain_ptls = N_PTLS;
// }

//double DistExpMS::get_entropy_gain(const std::vector<double> &controls) {
//    Eigen::MatrixXd Likelihoods; Likelihoods.resize(Result::NUM_RESULTS, m_gain_ptls);
//    Eigen::VectorXd MarginalLikelihoods; MarginalLikelihoods.resize(Result::NUM_RESULTS);
//
//    for (int r=0; r<Result::NUM_RESULTS; ++r) {
//        Eigen::MatrixXd InputPoints, Particles;
//
//        generate_particles(m_gain_ptls, controls, InputPoints, Particles);
//
//        Eigen::VectorXd likelihoods_row {Likelihoods.row(r)};
//        get_likelihoods(r, InputPoints, likelihoods_row);
//        Likelihoods.row(r) = likelihoods_row;
//        MarginalLikelihoods(r) = Likelihoods.row(r).sum()/m_gain_ptls;
//    }
//
//    // debugging
//    // std::cout << "expected (negative) entropy: ";
//    // std::cout << -MarginalLikelihoods.array().log().sum() + Likelihoods.array().log().sum()/m_gain_ptls << "\n";
//
//    // Note: in the expression below we have neglected a term in the approximation of the entropy gain, namely:
//    // sum_i { w_i*log(w_i) }. We can neglect this term since it's independent of the controls.
//
//    // Actually this is not even an approximation of the entropy gain; rather it's just the negative of the expected
//    // entropy after the next shot given the controls. Of course maximising this is equivalent to maximising information
//    // gain. This utility function is thus always negative (although I'm not sure that the approximation in terms of
//    // particles will always be, especially considering that we have neglected a negative term in the expression...).
//
//    return -MarginalLikelihoods.array().log().sum() + Likelihoods.array().log().sum()/m_gain_ptls;
//}

//double DistExpMS::get_NV(const std::vector<double> &controls) {
//    // assume scaling matrix is diagonal and compute it as a vector
//    Eigen::VectorXd scale; scale.resize(m_ndims);
//    scale(Param::DELTA) = 1.0/m_Mu(0); scale(Param::DELTA0) = scale(Param::DELTA);
//    if (m_3D) scale(Param::TAU) = 1.0/m_Mu(Param::TAU);
//
//    double utility {0.0};
//    for (int r=0; r<Result::NUM_RESULTS; ++r) {
//        Eigen::MatrixXd InputPoints, Particles;
//
//        generate_particles(m_gain_ptls, controls, InputPoints, Particles);
//        Eigen::VectorXd likelihoods; likelihoods.resize(m_gain_ptls);
//        get_likelihoods(r, InputPoints, likelihoods);
//
//        const double KAPPA {likelihoods.maxCoeff()};
//
//        Eigen::VectorXd Mu_acc; Mu_acc.resize(m_ndims);
//        Eigen::VectorXd Var_acc; Var_acc.resize(m_ndims);
//        Mu_acc.setZero(); Var_acc.setZero();
//        int N_acc {0};
//        for (int p=0; p<m_gain_ptls; ++p) {
//            if (likelihoods(p) > KAPPA*rand_unif()) { // accumulate
//                Mu_acc += Particles.col(p);
//                for (int i=0; i<m_ndims; ++i) {
//                    Var_acc(i) += Particles(i,p)*Particles(i,p);
//                }
//                ++N_acc;
//            }
//        }
//
//        if (N_acc < m_MIN_PTLS) {
//            std::cout << "Warning: less than " << m_MIN_PTLS << " particles were accumulated; cannot estimate negative variance.\n";
//            return -99.0;
//        } else {
//            Eigen::VectorXd Mu {Mu_acc/N_acc};
//            Eigen::VectorXd Neg_Vars; Neg_Vars.resize(m_ndims);
//            for (int i=0; i<m_ndims; ++i) {
//                Neg_Vars(i) = -scale(i)*( Var_acc(i)/N_acc - Mu(i)*Mu(i) );
//            }
//            utility += likelihoods.mean()*Neg_Vars.sum();
//        }
//    }
//    return utility;
//}

}
