#include "particle_dist.h"

#include <vector>
#include <stdexcept>
#include <iostream>

enum Data_type {
    GG_BIG,
    EE_BIG,
    GG_ACC,
    EE_ACC
};

pdist::DistExpMS dist;
int sequence_exp_num = 0;
std::vector<int> results;

namespace gl {
    double r_f_diff_offset = 0;
    double r_f_com_offset = 0;
    double r_time = 0;
    unsigned r_threshold_low_med_2Ca = 20;
    unsigned r_threshold_med_high_2Ca = 40;

}

void add_data(const api::compressed_data &data_struct, const int DATA_ID) {
    int result;
    bool small;
    std::vector<double> mins;
    std::vector<double> maxes;
    switch (DATA_ID) {
    case GG_BIG:
        result = pdist::Result::GG;
        small = false;
        mins = {0.1, -2.0, 0.0};
        maxes = {6.1,  2, 1.99501247};
        break;
    case EE_BIG:
        result = pdist::Result::EE;
        small = false;
        mins = {0.1, -2.0, 0.0};
        maxes = {6.1,  2, 1.99501247};
        break;
    case GG_ACC:
        result = pdist::Result::GG;
        small = true;
        mins = {1.5, -0.5, 0.25};
        maxes = {2.5, 0.5, 0.75};
        break;
    case EE_ACC:
        result = pdist::Result::EE;
        small = true;
        mins = {1.5, -0.5, 0.25};
        maxes = {2.5, 0.5, 0.75};
        break;
    default:
        throw std::runtime_error("Invalid Data_type");
    }
    const int SPACING {static_cast<int>(data_comp::Spacing::LIN)};
    std::vector<int> spacings = {SPACING, SPACING, SPACING};
    dist.set_data(result, data_struct, spacings, mins, maxes, small);
}

void particle_guess_heuristic(const std::vector<double> &Mu_in, const std::vector<std::vector<double>> &Sigma_in,
                              std::vector<double> &sample_out) {
    const int NDIMS {static_cast<int>(Mu_in.size())};
    Eigen::VectorXd Mu;
    Mu.resize(NDIMS);
    for (int i=0; i<NDIMS; ++i) {
        Mu(i) = Mu_in[i];
    }
    Eigen::MatrixXd Sigma;
    Sigma.resize(NDIMS, NDIMS);
    for (int i=0; i<NDIMS; ++i) {
        for (int j=0; j<NDIMS; ++j) {
            Sigma(i,j) = Sigma_in[i][j];
        }
    }

    Eigen::MatrixXd Sample;
    bool positive {pdist::multi_norm(1, Mu, Sigma, Sample)};
    if (!positive) std::cout << "Warning: Sigma is not positive definite.";
    sample_out.resize(NDIMS);
    for (int i=0; i<NDIMS; ++i) {
        sample_out[i] = Sample(i, 0);
    }
}
void set_gate_params(const std::vector<double> &Mu_in, const std::vector<std::vector<double>> &Sigma_in,
        double t_shaped_offset) {
    // choose initial gate parameters
    std::vector<double> sample;
    particle_guess_heuristic(Mu_in, Sigma_in, sample);
    gl::r_f_diff_offset = sample[0]/(2*M_PI);
    gl::r_f_com_offset = sample[1]/(2*M_PI);
//    gl::r_freq_729a_ms_gate_delta_2Ca.setValue( sample[0]/(2*M_PI) );
//    gl::r_freq_729a_carrier_MS_2Ca.setValue( sample[1]/(2*M_PI) );

    // Pick another random sample to compute the pulse time; this way the pulse time used is not perfectly correlated
    // with the detuning:
    double t_pulse {160.0};
    while (t_pulse > 150.0 || t_pulse < 6.) { // should double check these values
        particle_guess_heuristic(Mu_in, Sigma_in, sample);
        t_pulse = 2*M_PI/sample[0] + t_shaped_offset;
    }
    gl::r_time = t_pulse;
//    gl::r_t_729_ms_gate_2Ca.setValue( t_pulse );
}

void init(double delta_est=0, double delta_std=1, double delta0_est=0, double delta0_std=1, double t_shaped_offset=5.0) {

    // hard coding ranges for now
    std::vector<std::vector<double>> ranges = {{0.1, 6.1}, {-2.0, 2.0}, {0.0, 1.99501247}};
    dist.set_ranges(ranges, false);
    std::vector<std::vector<double>> ranges_small = {{1.5, 2.5}, {-0.5, 0.5}, {0.25, 0.75}};
    dist.set_ranges(ranges_small, true);

    // set initial distribution
    std::vector<double> Mu {2*M_PI*delta_est, 2*M_PI*delta0_est};
    std::vector<std::vector<double>> Sigma = {{4*M_PI*M_PI*delta_std*delta_std, 0.0},
                                              {0.0, 4*M_PI*M_PI*delta0_std*delta0_std}};

    dist.set_Mu(Mu); dist.set_Sigma(Sigma);

    set_gate_params(Mu, Sigma, t_shaped_offset);

    results.resize(pdist::Result::NUM_RESULTS);
    results[pdist::Result::GG] = 0; results[pdist::Result::EE] = 0; results[pdist::Result::GEEG] = 0;
    sequence_exp_num = 1;
}

unsigned get_pmt_channel_data_stub(unsigned channel_idx) {
    return 0;
}
void update_detection_counts(const std::vector<unsigned> &counts) {}

void get_results() {
    unsigned nAllCounts = get_pmt_channel_data_stub(0);
    unsigned nBkgCountsCa = 0;
    nBkgCountsCa += get_pmt_channel_data_stub(1);
    update_detection_counts({nAllCounts, nBkgCountsCa});

    // Bin the counts based on user-defined thresholds
    if (nAllCounts > gl::r_threshold_med_high_2Ca){
    	++results[pdist::Result::GG];
    } else if (nAllCounts > gl::r_threshold_low_med_2Ca){
    	++results[pdist::Result::GEEG];
    } else {
    	++results[pdist::Result::EE];
    }
}

bool post_sequence_calcs(unsigned iteration, unsigned max_iteration, double delta_std, double delta0_std, double scaling,
        bool update_params, double t_shaped_offset) {
    // operations after every sequence
    const int SHOT {static_cast<int>(iteration) + 1};
    std::cout << "Shot: " << SHOT << std::endl;

    int num_particles;
    if (SHOT <= 100) {
        // num_particles = 600;
        num_particles = 1200;
    } else if (SHOT <= 300) {
        // num_particles = 1500;
        num_particles = 3000;
    } else if (SHOT <= 500) {
        // num_particles = 3000;
        num_particles = 6000;
    } else if (SHOT <= 1000) {
        // num_particles = 15000;
        num_particles = 30000;
    } else {
        // haven't tried this before
    	// num_particles = 30000;
        num_particles = 60000;
    }

    std::vector<double> Mu;
    dist.get_Mu(Mu);
    std::vector<std::vector<double>> Sigma;
    dist.get_Sigma(Sigma);
    const double VAR_D  {(4*M_PI*M_PI*delta_std*delta_std/SHOT)/Sigma[0][0]};
    const double VAR_D0 {(4*M_PI*M_PI*delta0_std*delta0_std/SHOT)/Sigma[1][1]};
    const double VAR_MAX {(VAR_D > VAR_D0) ? VAR_D : VAR_D0};
    const double CONVERGENCE {pow(SHOT, scaling)};
    const double UNCERTAINTY {(CONVERGENCE*VAR_MAX > 1.0) ? CONVERGENCE*VAR_MAX : 1.0};

    std::vector<double> inputs; // current gate settings [delta, delta0, time]
    inputs.push_back(2*M_PI* gl::r_f_diff_offset);
    inputs.push_back(2*M_PI* gl::r_f_com_offset);
    inputs.push_back(gl::r_time - t_shaped_offset);
//    inputs.push_back(2*M_PI*gl::r_freq_729a_ms_gate_delta_2Ca);
//    inputs.push_back(2*M_PI*gl::r_freq_729a_carrier_MS_2Ca);
//    inputs.push_back(gl::r_t_729_ms_gate_2Ca - t_shaped_offset);

    // debugging
    // std::cout << "Sigma:\n";
    // for (int iii=0; iii<2; ++iii) {
    //     for (int jjj=0; jjj<2; ++jjj) {
	//         std::cout << " " << Sigma[iii][jjj];
    //     }
	//     std::cout << std::endl;
    // }
    // std::cout << "inputs: " << inputs[0] << " " << inputs[1] << " " << inputs[2] << std::endl;
    // std::cout << "UNCERTAINTY: " << UNCERTAINTY << std::endl;
    // std::cout << "results: " << results[0] << " " << results[1] << " " << results[2] << std::endl;
    // std::cout << "nBBSum: " << nBBSum << ", nBDSum" << nBDSum << ", nDDSum" << nDDSum << std::endl;

    bool update_successful {dist.update(results, inputs, num_particles, 1.0, UNCERTAINTY)};

    // Should eventually replace the line below with more appropriate error handling
    if (!update_successful) throw std::runtime_error("Update failed; too many particles outside dataset.");

    // setting a hard limit on the number of shots performed
    const int MAX_SHOTS {5000};
    if (SHOT > MAX_SHOTS) {
        iteration = max_iteration + 1;
    }

    dist.get_Mu(Mu);
    if (SHOT >= max_iteration || SHOT > MAX_SHOTS) {// calibration sequence has finished
        std::cout << "Total number of shots performed: " << SHOT << std::endl;
        const double MOT_DET {Mu[0]/(2*M_PI)};
        const double CAR_DET {Mu[1]/(2*M_PI)};
        const double T_PULSE {2*M_PI/Mu[0] + t_shaped_offset};
        std::cout << "Optimal parameters found:\n";
        std::cout << "Motional detuning: " << MOT_DET << "\n";
        std::cout << "Carrier detuning: " << CAR_DET << "\n";
        std::cout << "Gate time: " << T_PULSE << "\n";
        if (update_params) {
            dist.get_Mu(Mu);
            gl::r_f_diff_offset = MOT_DET;
            gl::r_f_com_offset = CAR_DET;
            gl::r_time = T_PULSE;
            // updateInGui(*gl::r_f_diff_offset);
            // updateInGui(*gl::r_f_com_offset);
            // updateInGui(*gl::r_time);

//            gl::r_freq_729a_ms_gate_delta_2Ca.setValue( MOT_DET );
//            gl::r_freq_729a_carrier_MS_2Ca.setValue( CAR_DET );
//            gl::r_t_729_ms_gate_2Ca.setValue( T_PULSE );
//            updateInGui(gl::r_freq_729a_ms_gate_delta_2Ca);
//            updateInGui(gl::r_freq_729a_carrier_MS_2Ca);
//            updateInGui(gl::r_t_729_ms_gate_2Ca);
        }
        return false;
    }

    results[pdist::Result::GG] = 0; results[pdist::Result::EE] = 0; results[pdist::Result::GEEG] = 0;
    dist.get_Sigma(Sigma);
    set_gate_params(Mu, Sigma, t_shaped_offset);

    if (SHOT < 100) {
        sequence_exp_num = 1;
    } else if (SHOT < 300) {
        sequence_exp_num = 5;
    } else if (SHOT < 500) {
        sequence_exp_num = 10;
    } else if (SHOT < 1000) {
        sequence_exp_num = 50;
    } else {
        // haven't tried this before
        sequence_exp_num = 100;
    }

    // return true to change the sequence (in particular if we return true, QubitManipulation() will be called before
    // the next shots)
    return true;
}

// Program the control system with the desired pulse sequence
// void set_sequence() {
//     DefaultCool();
//     StatePreparation();
//     QubitManipulation();

//     // Queue up detection and Doppler cooling at the end
//     DefaultDetect();
//     if (Ca2->rpbPrecoolLeaveOn){
//         DefaultCoolLeaveOn();
//     }

//     ProgramSequence();
// }

// Trigger a single execution of the currently loaded pulse sequence
// void trigger_sequence() {
// }

int main(int argc, char *argv[]) {
    // add_data(struct_from_Brennan, GG_ACC);
    
    const unsigned cycles = 100;

    init();
    // set_sequence();
    for (unsigned k=0; k < cycles; ++k) {

        // trigger_sequence();
        get_results();
        --sequence_exp_num;
        if (!sequence_exp_num) {
            if (post_sequence_calcs(k, cycles, 0, 0, 1, true, 5.0)) {
                // set_sequence();
            }
        }
    }


    return 0;
}
