# Bayesian calibration of an MS gate

This repository contains Brennan's MS gate calibration algorithm from ionpulse\_sdk\_core.
A short test.cpp file has been added to turn it into a compilable executable.

## Installation
Install Eigen3 fftw3 and boost

For Ubuntu 22.04 that would be
```sh
apt install libboost-dev libfftw3-dev libeigen3-dev
```

## Execution
The application won't run because the likelihood data is missing.
