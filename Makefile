.DEFAULT_GOAL := build/test_bcal_ms

build/test_bcal_ms:
	mkdir -p $(@D) && \
	cd $(@D) && \
	cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -GNinja ../src/ && \
	ninja
